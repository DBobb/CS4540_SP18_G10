﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontLetRigMove : MonoBehaviour {
    Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponentInParent<Rigidbody>();

    }
	
	// Update is called once per frame
	void Update () {
        this.transform.position = rb.position;
	}
}
