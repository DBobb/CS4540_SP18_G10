﻿using UnityEngine;


    public class GameOverManager : MonoBehaviour
    {
        CharacterHealth characterHealth;       // Reference to the player's health.


        Animator anim;                          // Reference to the animator component.


        void Awake ()
        {
            // Set up the reference.
            anim = GetComponent <Animator> ();
          //  characterHealth = GetComponent<PlayerManager>().curChar.GetComponent<CharacterHealth>();
        }


        void Update ()
        {
            // If the player has run out of health...
           // if(characterHealth.currentHealth <= 0)
            {
                // ... tell the animator the game is over.
              //  anim.SetTrigger ("GameOver");
            }
        }
    }
