﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class SetFocus : MonoBehaviour {

    GameObject focusButton;
	// Update is called once per frame
	void Start () {
        focusButton = GameObject.Find("NewGame");
        EventSystem.current.SetSelectedGameObject(focusButton);
    }
}
