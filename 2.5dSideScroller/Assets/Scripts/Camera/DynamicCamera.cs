﻿using UnityEngine;
using System.Collections;

// Author: Drew Bobbitt

// Time spent: 1 hours

// User Story: N/A

// Class Purpose: Creating a functional camera that lags slightly behind player before catching up.

public class DynamicCamera : MonoBehaviour {

	//Initializing variables
	public Transform target; //The position of the object the camera is following
	public float smoothing = 5f; //smoothing rate, value can be adjusted

	Vector3 offset; 

	// Use this for initialization
	//Initializing offset
	void Start () {
		offset = transform.position - target.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//This function updates the camera's position and applies the lag behind effect
	void FixedUpdate(){
		Vector3 targetCamPos = target.position + offset;

		//Vector lerp
		transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
	}
}
