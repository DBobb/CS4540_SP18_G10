﻿// Author: Keaton Smith
// Time spent: 1 hours
// User Story: 29
// Class Purpose: This script is to be attached to the hitbox colliders that are attached to character objects
// The functionality of this is essentially just an On TriggerEnter function
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHit : MonoBehaviour {

    public float damage = 0;

    SpriteRenderer hitSprite;
    bool isPaused = false;
	// Use this for initialization
	void Start () {
       hitSprite = GetComponentInChildren<SpriteRenderer>();
       hitSprite.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (isPaused == true) hitSprite.enabled = false;

        if (other.tag == "Enemy")
        {
            hitSprite.transform.position = (other.transform.position + transform.position)/2;
            other.GetComponent<EnemyHealth>().TakeDamage(damage);
            PauseAndResume();
        }
		
    }
    void PauseAndResume()
    {
        Time.timeScale = 1f;
        //Display Image here
        StartCoroutine(ResumeAfterNSeconds(0.25f));
    }

    float timer = 0;
    IEnumerator ResumeAfterNSeconds(float timePeriod)
    {
        hitSprite.enabled = true;
        yield return new WaitForEndOfFrame();
        timer += Time.unscaledDeltaTime;
        if (timer < timePeriod)
        {
            StartCoroutine(ResumeAfterNSeconds(timePeriod));
        }
        else
        {
            Time.timeScale = 1;                //Resume
            hitSprite.enabled = false;
            timer = 0;
        }
    }

}
