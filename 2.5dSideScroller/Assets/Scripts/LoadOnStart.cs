﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadOnStart : MonoBehaviour {

    // Use this for initialization
    private void OnLevelWasLoaded(int level)
    {
        SaveManager.Instance.Load();
        SaveManager.Instance.Save();
    }

}
