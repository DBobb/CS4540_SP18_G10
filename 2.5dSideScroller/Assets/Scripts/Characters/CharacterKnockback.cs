﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterKnockback : MonoBehaviour {

    // Amount of time in seconds the player is not in control
    public float timeInKnockback = 0.5f;
    public int distanceKnockedBack = 10;

    float timer = 0f;
    public bool beingKnockedBack = false;

    Rigidbody myRB;
    Animator myAnim;
    CharacterMovement myCM;
    GameObject myPM;
    
    // Use this for initialization
    void Start () {
        //myPM = GameObject.FindGameObjectWithTag("PlayerManager");

        myRB = gameObject.GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
        myCM = gameObject.GetComponent<CharacterMovement>();
    }
	
	// Update is called once per frame
	void Update () {
        if (beingKnockedBack)
        {
            timer += Time.deltaTime;
        }

        if (beingKnockedBack && timer >= timeInKnockback) // if the player is being knocked back and the time passing has exceeded the max time in knockback
        {
            timer = 0f;
            beingKnockedBack = false;
            myAnim.SetBool("Hit", false);
            myCM.enabled = true;
        }
    }

    // Functions to be used as Coroutines MUST return an IEnumerator
    public void TakeKnockback(Vector3 enemyPos)
    {
        float direc = Mathf.Sign(myRB.position.x - enemyPos.x); // Gets a negative or positive direction to send the player depending on relative positions of enemy to character
        beingKnockedBack = true;
        myAnim.SetBool("Hit", true);

        gameObject.GetComponent<Rigidbody>().velocity = new Vector3(direc * distanceKnockedBack, distanceKnockedBack, 0);
        myCM.enabled = false;
    }
    
}
