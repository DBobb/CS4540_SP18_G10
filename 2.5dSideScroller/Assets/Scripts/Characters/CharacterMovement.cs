﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    // ****** Movement Variables ******
    public float runSpeed = 10f;
    public bool canMove = true;
    public int side; // -1 for facing left, +1 for facing right

    Rigidbody myRB;
    Animator myAnim;
    bool facingRight;

    // Use this for initialization
    void Start()
    {

        myRB = GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
        facingRight = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        GetInputs();
    }


    //This function simply flips the player model about the z axis
    void FlipModel()
    {
        facingRight = !facingRight; //change our facing boolean to reflect current direction we're facing
        Vector3 modelScale = transform.localScale;
        modelScale.z *= -1;
        transform.localScale = modelScale;
    }

    void GetInputs()
    {
        float move = Input.GetAxis("Horizontal");
        myAnim.SetFloat("hspeed", Mathf.Abs(move));
        if (canMove) myRB.velocity = new Vector3(move * runSpeed, myRB.velocity.y, 0);

        // Using Inputs and Hspeed to Filp model in animator
        if (move > 0 && !facingRight)
            FlipModel();
        else if (move < 0 && facingRight)
            FlipModel();
    }

    public float GetFacing()
    {
        if (facingRight)
            return 1;
        else
            return -1;
    }

    public void SetCanMove(bool result)
    {
        canMove = result;
    }
}
