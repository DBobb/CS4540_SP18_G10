﻿// Author: Drew Bobbitt, refactored later by Keaton Smith
// Time spent: 8 hours + 4 hours
// User Story: 46
// Class Purpose: Allows Character objects to jump

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJump : MonoBehaviour {

   // public float jumpForce;
    public float maxJumpHeight = 100;
    public float minJumpHeight = 50;

    float maxJumpForce;
    float minJumpForce;
    float timeToApex = 0.5f;
    float timeToRelease = 0.2f;
    float timeSinceJump = 0f;

    Rigidbody myRB;
    Animator myAnim;
    CharacterGrounded characterGrounded;
    bool grounded;

    bool doMaxJump;
    bool doMinJump;
    bool initiateJump;
    // Jump buffer variables
    bool bufferPressed = false;
    float buffer = 0.3f; // Amount of time the player can input Jump before landing and immediately jump upon contact
    float buffTimer = 0f;
    bool canJump = true;
    // Use this for initialization
    void Start () {
        myRB = GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
        characterGrounded = GetComponent<CharacterGrounded>();
        grounded = characterGrounded.grounded;
        float grav = Physics.gravity.y;
        minJumpForce = Mathf.Sqrt(2 * Mathf.Abs(grav) * minJumpHeight);
        maxJumpForce = Mathf.Sqrt(2 * Mathf.Abs(grav) * maxJumpHeight);
    }

    // Update is called once per frame
    void Update () {

        if (canJump)
        {
            grounded = characterGrounded.grounded;

            if (Input.GetButtonDown("Jump") && grounded)
            {
                doMaxJump = true;
            }
            if (Input.GetButtonUp("Jump") && !grounded)
            {
                doMinJump = true;
            }
        }
    }

    void FixedUpdate()
    {
        grounded = characterGrounded.grounded;
        JumpBuffer();
        CharJump();
    }


    void CharJump()
    {
        // Getting a buffered jumpkey press before landing
        if (grounded && ((doMinJump || doMaxJump) || bufferPressed))
        {

            grounded = false;
            myAnim.SetBool("grounded", grounded);

            doMaxJump = true;
            //initiateJump = true;

            timeSinceJump = 0f;

            bufferPressed = false;
            buffTimer = 0f;
        }


        // Code for variable jumping
        if (doMaxJump && timeSinceJump < timeToRelease && !doMinJump) // if The player hit jump button on the ground, and the timer for them to release the jump button has not timed out, and the user has not released the jump button, add the max force until time runs out.
        {
            timeSinceJump += Time.deltaTime;
            if (!(Input.GetAxisRaw("Jump") > 0))
            {
                doMaxJump = false;
                doMinJump = true;
            }
            myRB.velocity = (new Vector3(0, maxJumpForce, 0));
        }
        else if (!Input.GetButtonDown("Jump") && timeSinceJump < timeToRelease && doMinJump)// else if the player is not holding down jump, and the timer has been set, and the timer to release has not timed out, add the minforce until the time runs out
        {
            myRB.velocity = (new Vector3(0, minJumpForce, 0));
            timeSinceJump += Time.deltaTime;
            
            doMaxJump = false;
        }
        else if (timeSinceJump >= timeToRelease) // else if the timer has exceeded the time to 
        {
            doMaxJump = false;
            doMinJump = false;
        }

    }
    
    // This function sets whether or not the player buffered the jump button before hitting the ground
    void JumpBuffer()
    {

        if (!grounded && Input.GetButtonDown("Jump"))
        {
            bufferPressed = true;
        }
        if (bufferPressed)
        {
            buffTimer += Time.deltaTime;
            if (buffTimer > buffer)
            {
                bufferPressed = false;
            }
        }

    }

    public void SetCanJump(bool result)
    {
        canJump = result;
    }
}
