﻿// Author: Keaton Smith
// Time spent: 8 hours
// User Story: 25
// Class Purpose: This script allows the characters to have and maintain health. Also contains 
// a public method for taking damage from enemy objects. Also contains method for death event.
// Has commented out code for future events regarding death animations and audio clips. This 
// script also can restore the player's health when using calcite.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
public class CharacterHealth : MonoBehaviour
{
    public int healthPotionValue;
    public int startingHealth = 100;                            // The amount of health the player starts the game with.
    public int currentHealth;                                   // The current health the player has.
    public Image damageImage;                                   // Reference to an image to flash on the screen on being hurt.
    public AudioClip deathClip;                                 // The audio clip to play when the player dies.
	public bool invincible = false;

	//Vector3 respawnPosition = GameObject.Find("Rock_Crystals_Big_1").transform.position;

    Animator anim;                                              // Reference to the Animator component.
    AudioSource playerAudio;                                    // Reference to the AudioSource component.
    CharacterMovement playerMovement;                           // Reference to the player's movement.
    public bool isDead = false;                                        // Whether the player is dead.
    bool damaged;                                               // True when the player gets damaged.

    GameObject player;
    GameObject curChar;
    CharacterKnockback myCK;

    void Awake()
    {

            // Setting up the references.
            anim = GetComponent <Animator> ();
            playerAudio = GetComponent <AudioSource> ();
            playerMovement = GetComponent <CharacterMovement> ();


           // player = GameObject.FindGameObjectWithTag("PlayerManager");
           // pm = player.GetComponent<PlayerManager>();
           // curChar = pm.curChar;

			curChar = CharacterManager.Instance.curChar;
            myCK = gameObject.GetComponent<CharacterKnockback>();
            // Set the initial health of the player.
            currentHealth = startingHealth;

    }
    void Update()
    {
        UIManager.Instance.curCharHealthSlider.value = currentHealth;
        // Update who the current character is based on PlayerManager
        //curChar = pm.curChar;
        //curChar = CharacterManager.Instance.curChar;
        // If the player has just been damaged...
        if (damaged)
        {
                // ... set the colour of the damageImage to the flash colour.
                //damageImage.color = flashColour;
        }
            // Otherwise...
        else
        {
                // ... transition the colour back to clear.
                //damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        // Reset the damaged flag.
        damaged = false;
    }
    public void TakeDamage(int amount)
    {
        // Set the damaged flag so the screen will flash.
        damaged = true;

        // Reduce the current health by the damage amount.
        currentHealth -= amount;
        // Set the health bar's value to the current health.
        //pm.curCharHealthSlider.value = currentHealth;

        // Play the hurt sound effect.
        //playerAudio.Play ();
        //gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(-10, 10, 0));

		UIManager.Instance.curCharHealthSlider.value = currentHealth;
        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (currentHealth <= 0 && !isDead)
        {
            // ... it should die.
            Death();
        }
    }

    void Death()
    { 
            // Set the death flag so this function won't be called again.	
		if (CharacterManager.Instance.curChar == CharacterManager.Instance.char1) {
            //gameObject.transform.position = respawnPosition;

            Invoke("ReloadLevel", 1);

        }
        else
        {
            Invoke("SetDead", 1);
            
        }

        // Turn off any remaining shooting effects.
        //playerShooting.DisableEffects ();

        // Tell the animator that the player is dead.
        //anim.SetTrigger ("Die");

        // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
        //playerAudio.clip = deathClip;
        //playerAudio.Play ();

        // Turn off the movement and shooting scripts.
        //playerMovement.enabled = false;
        //playerShooting.enabled = false;

    }

    void SetDead()
    {
        this.isDead = true;
    }
    void ReloadLevel()
    {
        GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().LoseSoftBismuth();
        SceneManager.LoadScene("Scene03");
        SaveManager.Instance.Load();
    }
        


    // This function adds the set calcite potion value to the current
    // character's health
    public void AddHealthPotion()
    {
        // If the current health is not full, add the health potion value
        // to current health
        if (currentHealth < startingHealth && currentHealth > 0)
        {
            // If the health potion value will overfill the player's health,
            // only add the value that will make the current health full
            if((currentHealth + healthPotionValue) > startingHealth)
            {
                healthPotionValue = startingHealth - currentHealth;
                currentHealth = currentHealth + healthPotionValue;
				UIManager.Instance.curCharHealthSlider.value = currentHealth;
            }

            else
            {
                currentHealth = currentHealth + healthPotionValue;
				UIManager.Instance.curCharHealthSlider.value = currentHealth;
            }
        }
    }
}