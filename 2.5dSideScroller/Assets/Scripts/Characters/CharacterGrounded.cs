﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGrounded : MonoBehaviour {

    Rigidbody myRB;
    Animator myAnim;

    // ****** Jumping Variables ******
    public bool grounded;
    Collider[] groundCollisions;
    float groundCheckRadius = 0.2f;
    //public LayerMask groundLayer;
   // public Transform groundCheck;

    // Use this for initialization
    void Start () {
        myRB = GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //  CheckGrounded();
       
        myAnim.SetBool("grounded", grounded);
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "ground")
        {
            grounded = true;
        }
        
    }

    void OnCollisonStay(Collision col)
    {
        if(col.gameObject.name == "ground")
        {
            grounded = true;
        }
        
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.name == "ground")
        {
            grounded = false;
        }
        
    }



    /*
    void CheckGrounded()
    {
        //Checking for ground collisions
        groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);
        if (groundCollisions.Length > 0)
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
        //Assigning the grounded variable found to the grounded variable in the player animator
        myAnim.SetBool("grounded", grounded);

    }*/
}
