﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 7 hours
// User Story: 24 and 27
// Class Purpose: Creates a list of lode stone points, allows the player to be brought 
//                back to the last lode stone visited, and regenerate stats at any lodestone
public class LodeStonePoints : MonoBehaviour {

    // Variables that track the lodestones
    GameObject lodeStoneManager;
    GameObject lodeStoneChild;
    int lodeStoneCount;
    int index;

    public List<GameObject> lodeStoneList;
    public List<bool> visitedLodeStone;

	// This function creates the lode stone lists on awake and counts the current lodestones
	void Awake () {
        lodeStoneManager = GameObject.Find("Lodestones");
        lodeStoneCount = lodeStoneManager.transform.childCount;

        lodeStoneList = new List<GameObject>();
        visitedLodeStone = new List<bool>();

        for(int i = 0; i < lodeStoneCount; i++){
            lodeStoneChild = lodeStoneManager.transform.GetChild(i).gameObject;
            lodeStoneList.Add(lodeStoneChild);
            visitedLodeStone.Insert(i, false);
            
        }
        visitedLodeStone.Insert(0, true);
	}
	
	// This function updates the list if a lodestone has been visited by the player
	public void HasVisited (GameObject lodeStone) {
        index = lodeStoneList.FindIndex(x => x == lodeStone);
        visitedLodeStone[index] = true;
	}
}
