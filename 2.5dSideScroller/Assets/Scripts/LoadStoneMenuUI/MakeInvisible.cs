﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeInvisible : MonoBehaviour {

    Transform panelTrans;
    int childCount; 

    public void BecomeInvisible(){
        panelTrans = GameObject.Find("OverlayPanel").transform;
        childCount = panelTrans.childCount;

        for (int i = 0; i < childCount; i++){
            panelTrans.GetChild(i).gameObject.GetComponent<Canvas>().enabled = false;
        }
    }
}
