﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class ShowConvert : MonoBehaviour {
    public int index;
    int sbWorth = 10;
    string itemToRemove;
    bool ableToAdd = false;

    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == this.gameObject)
        {
            index = GameObject.Find("ItemBox").GetComponent<MainInventory>().chooseAllSlots.FindIndex(x => x == this.gameObject && x.name == this.gameObject.name);
            if (GameObject.Find("ItemBox").GetComponent<MainInventory>().slotStatus[index] == false)
            {
                GameObject.Find("ConvertBox").transform.GetChild(1).transform.GetChild(1).GetComponent<Text>().text = sbWorth.ToString();
            }
            else
            {
                GameObject.Find("ConvertBox").transform.GetChild(1).transform.GetChild(1).GetComponent<Text>().text = "0";
            }

            if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == this.gameObject)
            {
                if (GameObject.Find("ItemBox").GetComponent<MainInventory>().slotStatus[index] == false)
                {
                    itemToRemove = GameObject.Find("ItemBox").GetComponent<MainInventory>().chooseAllSlots[index].transform.GetChild(0).GetComponent<Text>().text;

                    GameObject.Find("ItemBox").GetComponent<MainInventory>().slotStatus[index] = true;
                    GameObject.Find("ItemBox").GetComponent<AddToMainInv>().itemsList.Remove(itemToRemove);
                    GameObject.Find("ItemBox").GetComponent<MainInventory>().allSlots[index].transform.GetChild(0).GetComponent<Text>().text = "";
                    GameObject.Find("ItemBox").GetComponent<MainInventory>().chooseAllSlots[index].transform.GetChild(0).GetComponent<Text>().text = "";

                    GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount += sbWorth;
                    GameObject.Find("SoftBismuthCount").GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount.ToString();
                    GameObject.Find("ConvertBox").transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount.ToString();
                    GameObject.Find("ConvertBox").transform.GetChild(1).transform.GetChild(1).GetComponent<Text>().text = "0";
                }
            }
        }
           
    }
}
