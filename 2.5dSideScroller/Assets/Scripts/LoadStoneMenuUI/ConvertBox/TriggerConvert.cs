﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class TriggerConvert : MonoBehaviour, ISelectHandler{

    GameObject sidePanel;
    GameObject panel;
    GameObject itemBox;

    TriggerPointers trigPointers;

    Transform panelTrans;

    int childCount;

    public void OnSelect(BaseEventData eventData)
    {
        GameObject.Find("UIManager").GetComponent<MakeInvisible>().BecomeInvisible();

        sidePanel = GameObject.Find("SidePanel");
        trigPointers = sidePanel.GetComponent<TriggerPointers>();

        trigPointers.InitializePointer("ConvertToBismuth");

        itemBox = GameObject.Find("ItemBoxButton");
        itemBox.GetComponent<TriggerItemBox>().onSideMenu = false;

        GameObject.Find("ConvertBox").GetComponent<Canvas>().enabled = true;

        GameObject.Find("ConvertBox").transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount.ToString();
    }
}
