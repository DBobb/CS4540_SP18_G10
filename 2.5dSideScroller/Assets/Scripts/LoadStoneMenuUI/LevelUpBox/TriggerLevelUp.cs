﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class TriggerLevelUp : MonoBehaviour, ISelectHandler{
    public GameObject levelUpBox;
    GameObject sidePanel;
    
    Transform panelTrans;
    TriggerPointers trigPointers;

    bool onLevelUp = false;
    public void OnSelect(BaseEventData eventData){
        GameObject.Find("UIManager").GetComponent<MakeInvisible>().BecomeInvisible();

        sidePanel = GameObject.Find("SidePanel");
        trigPointers = sidePanel.GetComponent<TriggerPointers>();

        trigPointers.InitializePointer("LevelUp");
        levelUpBox.GetComponent<Canvas>().enabled = true;

        GameObject.Find("AmeliaStats").GetComponent<Canvas>().enabled = true;
        GameObject.Find("ItzuliStats").GetComponent<Canvas>().enabled = false;
        GameObject.Find("FlorenceStats").GetComponent<Canvas>().enabled = false;
    }
}
