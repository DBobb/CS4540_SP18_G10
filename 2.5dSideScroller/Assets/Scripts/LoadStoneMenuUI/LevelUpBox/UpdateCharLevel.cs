﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UpdateCharLevel : MonoBehaviour {
    // Update is called once per frame
    public int charLevel;
    void Start()
    {
        charLevel = 1;
        this.gameObject.GetComponent<Text>().text = charLevel.ToString();
    }
    void Update () {
        this.gameObject.GetComponent<Text>().text = charLevel.ToString();
    }
}
