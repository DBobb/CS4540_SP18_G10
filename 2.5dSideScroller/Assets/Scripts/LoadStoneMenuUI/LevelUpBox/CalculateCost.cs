﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CalculateCost : MonoBehaviour {
    int charLevel;
    public float startingCost;
    float cost;
    float rate;
    public int roundedCost;
    float ratePlusOne;

     void Awake()
    {
        startingCost = 10;
        this.transform.GetChild(1).GetComponent<Text>().text = startingCost.ToString();

    }
    public int CalcCost () {
        rate = 0.1f;
        charLevel = System.Convert.ToInt32(this.transform.parent.GetChild(1).GetComponent<Text>().text) + 1;

        ratePlusOne = rate + 1;
        cost = startingCost * (Mathf.Pow(ratePlusOne, charLevel));
        roundedCost = (Mathf.RoundToInt(cost));

        this.gameObject.transform.GetChild(1).GetComponent<Text>().text = roundedCost.ToString();
        return roundedCost;
    }
    public void UndoCost()
    {
        cost =  cost / (Mathf.Pow(ratePlusOne, charLevel));

    }
}
