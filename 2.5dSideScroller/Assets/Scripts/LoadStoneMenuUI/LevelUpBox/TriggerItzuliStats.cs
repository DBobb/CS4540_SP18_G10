﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class TriggerItzuliStats : MonoBehaviour, ISelectHandler {
    public void OnSelect(BaseEventData eventData)
    {
        GameObject.Find("AmeliaStats").GetComponent<Canvas>().enabled = false;
        GameObject.Find("ItzuliStats").GetComponent<Canvas>().enabled = true;
        GameObject.Find("FlorenceStats").GetComponent<Canvas>().enabled = false;
    }
}
