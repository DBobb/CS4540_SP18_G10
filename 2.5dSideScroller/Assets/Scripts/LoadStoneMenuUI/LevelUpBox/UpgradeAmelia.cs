﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 1 day 7 hours
// User Story: 30
// Class Purpose: Upgrades Amelia's stats, dependent on the stat the player chooses to upgrade.
public class UpgradeAmelia : MonoBehaviour {

    // Variables to upgrade Amelia's stats
    int roundedCost;
    int hpLevel, mpLevel,
        speedLevel, lightLevel,
        heavyLevel, invSizeLevel;
    int amountToUpgrade = 10;
    int softBismuthAmount;
    string buttonName;

    GameObject character;

    // This function is called on Awake and it sets default values of the levels
    void Awake(){
        hpLevel = 1;
        mpLevel = 1;
        speedLevel = 1;
        lightLevel = 1;
        heavyLevel = 1;
    }

    // This function checks if the player presses a button that is tied to Amelia's stats and then upgrades that stat
    // if they have enough soft bismuth
    void Update(){
        // Gets the players current soft bismuth amount
        softBismuthAmount = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount;

        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject.tag == "AmeliaStat"){
            buttonName = EventSystem.current.currentSelectedGameObject.name;

            // If the character's level is one, the cost is set to a default amount
            if (this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel == 1){
                roundedCost = System.Convert.ToInt32(this.gameObject.transform.GetChild(12).GetComponent<CalculateCost>().startingCost);
            }

            // If the player can afford to upgrade the stat, then do it
            if (GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount >= roundedCost){
                GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount - roundedCost;
                GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().softBismuthCount.GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount.ToString();

                // Upgrades Amelia's HP
                if (buttonName == "HP"){
                    hpLevel++;
                    this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel++;
                    this.gameObject.transform.GetChild(2).transform.GetChild(1).GetComponent<Text>().text = hpLevel.ToString();
                    CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth + amountToUpgrade;
                    UIManager.Instance.char2HealthSlider.GetComponent<Slider>().maxValue = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth + amountToUpgrade;
                    UIManager.Instance.char2HealthSlider.GetComponent<Slider>().value = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth + amountToUpgrade;
                }

                // Upgrades Amelia's MP
                else if (buttonName == "MP"){
                    mpLevel++;
                    this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel++;
                    this.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = mpLevel.ToString();
                    CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana + amountToUpgrade;
                    UIManager.Instance.Char2ManaSlider.GetComponent<Slider>().maxValue = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana + amountToUpgrade;
                    UIManager.Instance.Char2ManaSlider.GetComponent<Slider>().value = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana + amountToUpgrade;
                }

                // Upgrades Amelia's speed
                else if (buttonName == "Speed"){
                    speedLevel++;
                    this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel++;
                    this.gameObject.transform.GetChild(4).transform.GetChild(1).GetComponent<Text>().text = speedLevel.ToString();
                    CharacterManager.Instance.char2.GetComponent<CharacterMovement>().runSpeed = CharacterManager.Instance.char2.GetComponent<CharacterMovement>().runSpeed + amountToUpgrade;
                }

                /*else if (buttonName == "LightAttack")
                {
                    lightLevel++;
                    charLevel++;
                    this.gameObject.transform.GetChild(1).GetComponent<Text>().text = lightLevel.ToString();
                    CharacterManager.Instance.char2.GetComponent<CharacterLightAttack>().damage = CharacterManager.Instance.char2.GetComponent<CharacterLightAttack>().damage + amountToUpgrade;
                }
                else if (buttonName == "HeavyAttack")
                {
                    heavyLevel++;
                    charLevel++;
                    this.gameObject.transform.GetChild(1).GetComponent<Text>().text = heavyLevel.ToString();
                    CharacterManager.Instance.char2.GetComponent<CharacterHeavyAttack>().damage = CharacterManager.Instance.char2.GetComponent<CharacterHeavyAttack>().damage + amountToUpgrade;
                }*/
            }
            // If the player can't afford it, undo the calculation
            else {
                this.gameObject.transform.parent.transform.GetChild(12).GetComponent<CalculateCost>().UndoCost();
            }
            roundedCost = this.gameObject.transform.GetChild(12).GetComponent<CalculateCost>().CalcCost();
        }
    }
}

