﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class UpgradeFlorence : MonoBehaviour {
    float cost;
    int roundedCost;
    float ratePlusOne;
    float rate;
    int hpLevel, mpLevel,
        speedLevel, lightLevel,
        heavyLevel, invSizeLevel;
    int charLevel;
    int amountToUpgrade = 10;
    int softBismuthAmount;
    GameObject character;
    string buttonName;
    void Awake()
    {
        rate = 0.1f;
        hpLevel = 1;
        mpLevel = 1;
        speedLevel = 1;
        lightLevel = 1;
        heavyLevel = 1;
        invSizeLevel = 1;
        charLevel = 1;
    }
    void Update()
    {
        softBismuthAmount = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount;

        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject.tag == "FlorenceStat")
        {
            buttonName = EventSystem.current.currentSelectedGameObject.name;

            if (this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel == 1)
            {
                roundedCost = System.Convert.ToInt32(this.gameObject.transform.GetChild(12).GetComponent<CalculateCost>().startingCost);
            }

            if (GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount >= roundedCost)
            {
                GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount - roundedCost;
                GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().softBismuthCount.GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount.ToString();

                if (buttonName == "HP")
                {
                    hpLevel++;
                    this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel++;
                    this.gameObject.transform.GetChild(2).transform.GetChild(1).GetComponent<Text>().text = hpLevel.ToString();
                    CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth + amountToUpgrade;
                    UIManager.Instance.char3HealthSlider.GetComponent<Slider>().maxValue = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth + amountToUpgrade;
                    UIManager.Instance.char3HealthSlider.GetComponent<Slider>().value = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth + amountToUpgrade;
                }
                else if (buttonName == "MP")
                {
                    mpLevel++;
                    this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel++;
                    this.gameObject.transform.GetChild(3).transform.GetChild(1).GetComponent<Text>().text = mpLevel.ToString();
                    CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana + amountToUpgrade;
                    UIManager.Instance.Char3ManaSlider.GetComponent<Slider>().maxValue = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana + amountToUpgrade;
                    UIManager.Instance.Char3ManaSlider.GetComponent<Slider>().value = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana + amountToUpgrade;
                }
                else if (buttonName == "Speed")
                {
                    speedLevel++;
                    this.gameObject.transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel++;
                    this.gameObject.transform.GetChild(4).transform.GetChild(1).GetComponent<Text>().text = speedLevel.ToString();
                    CharacterManager.Instance.char3.GetComponent<CharacterMovement>().runSpeed = CharacterManager.Instance.char3.GetComponent<CharacterMovement>().runSpeed + amountToUpgrade;
                }
                /*else if (buttonName == "LightAttack")
                {
                    lightLevel++;
                    charLevel++;
                    this.gameObject.transform.GetChild(1).GetComponent<Text>().text = lightLevel.ToString();
                    CharacterManager.Instance.char2.GetComponent<CharacterLightAttack>().damage = CharacterManager.Instance.char2.GetComponent<CharacterLightAttack>().damage + amountToUpgrade;
                }
                else if (buttonName == "HeavyAttack")
                {
                    heavyLevel++;
                    charLevel++;
                    this.gameObject.transform.GetChild(1).GetComponent<Text>().text = heavyLevel.ToString();
                    CharacterManager.Instance.char2.GetComponent<CharacterHeavyAttack>().damage = CharacterManager.Instance.char2.GetComponent<CharacterHeavyAttack>().damage + amountToUpgrade;
                }*/
            }
            else
            {
                this.gameObject.transform.parent.transform.GetChild(12).GetComponent<CalculateCost>().UndoCost();
            }
            roundedCost = this.gameObject.transform.GetChild(12).GetComponent<CalculateCost>().CalcCost();
        }
    }
}
