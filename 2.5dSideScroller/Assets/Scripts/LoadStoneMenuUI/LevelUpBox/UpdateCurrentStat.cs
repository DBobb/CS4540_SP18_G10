﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UpdateCurrentStat : MonoBehaviour {

	void Update () {
        if (GameObject.Find("LevelUpBox").GetComponent<Canvas>().enabled == true){

            GameObject.Find("LevelUpBox").transform.GetChild(2).gameObject.GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount.ToString();

            if (GameObject.Find("AmeliaStats").GetComponent<Canvas>().enabled == true)
            {
                GameObject.Find("AmeliaStats").transform.GetChild(7).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth.ToString();
                GameObject.Find("AmeliaStats").transform.GetChild(8).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana.ToString();
                GameObject.Find("AmeliaStats").transform.GetChild(9).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char2.GetComponent<CharacterMovement>().runSpeed.ToString();
                //this.gameObject.transform.parent.GetChild(10).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char2.GetComponent<CharacterLightAttack>().damage.ToString();
                //this.gameObject.transform.parent.GetChild(11).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char2.GetComponent<CharacterHeavyAttack>().damage.ToString(); 
            }
            else if (GameObject.Find("ItzuliStats").GetComponent<Canvas>().enabled == true)
            {
                GameObject.Find("ItzuliStats").transform.GetChild(7).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char1.GetComponent<CharacterHealth>().startingHealth.ToString();
                GameObject.Find("ItzuliStats").transform.GetChild(8).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char1.GetComponent<CharacterMana>().startingMana.ToString();
                GameObject.Find("ItzuliStats").transform.GetChild(9).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char1.GetComponent<CharacterMovement>().runSpeed.ToString();
                GameObject.Find("ItzuliStats").transform.GetChild(10).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char1.GetComponent<CharacterLightAttack>().damage.ToString();
                GameObject.Find("ItzuliStats").transform.GetChild(11).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char1.GetComponent<CharacterHeavyAttack>().damage.ToString(); 
            }
            else if (GameObject.Find("FlorenceStats").GetComponent<Canvas>().enabled == true)
            {
                GameObject.Find("FlorenceStats").transform.GetChild(7).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth.ToString();
                GameObject.Find("FlorenceStats").transform.GetChild(8).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana.ToString();
                GameObject.Find("FlorenceStats").transform.GetChild(9).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char3.GetComponent<CharacterMovement>().runSpeed.ToString();
                //this.gameObject.transform.parent.GetChild(10).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char2.GetComponent<CharacterLightAttack>().damage.ToString();
                //this.gameObject.transform.parent.GetChild(11).gameObject.transform.GetChild(0).GetComponent<Text>().text = CharacterManager.Instance.char2.GetComponent<CharacterHeavyAttack>().damage.ToString();
            }
        }
    }
}
