﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class TriggerItemBox : MonoBehaviour, ISelectHandler{
    GameObject inventory;
    GameObject inventoryCopy;
    GameObject panel;
    GameObject sidePanel;
    GameObject player;
    public GameObject mainInventory;
    public GameObject inventoryPanel;

    Transform invPanelTrans;
    Transform panelTrans;
    TriggerPointers trigPointers;

    int childCount;
    public bool onSideMenu = false;
    public bool hasBeenClicked = false;

    public Vector3 oldInvSpot;

    public void OnSelect(BaseEventData eventData){
        GameObject.Find("UIManager").GetComponent<MakeInvisible>().BecomeInvisible();

        sidePanel = GameObject.Find("SidePanel");
        trigPointers = sidePanel.GetComponent<TriggerPointers>();
        trigPointers.InitializePointer("ItemBox");

        GameObject.Find("ItemBox").GetComponent<Canvas>().enabled = true;

        onSideMenu = true;

        if(hasBeenClicked == false){
            inventory = GameObject.Find("Inventory").transform.GetChild(0).gameObject;
            oldInvSpot = inventory.transform.localPosition;

            inventory.transform.SetParent(GameObject.Find("InventoryCanvas").transform);
            inventory.transform.localPosition = new Vector3(280,300, 0);
        }
        else { inventory.GetComponent<Canvas>().enabled = true; }

        hasBeenClicked = true;
    }
}
