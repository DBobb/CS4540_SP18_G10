﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainInventory : MonoBehaviour {
    public List<GameObject> allSlots;
    public List<GameObject> chooseAllSlots;
    public List<bool> slotStatus;

    private void Awake()
    {
        allSlots = new List<GameObject>();
        chooseAllSlots = new List<GameObject>();
        slotStatus = new List<bool>();

        for (int i = 1; i < GameObject.Find("MainInventory").transform.childCount; i++)
        {
            allSlots.Add(GameObject.Find("MainInventory").transform.GetChild(i).gameObject);
            chooseAllSlots.Add(GameObject.Find("ChooseItem").transform.GetChild(i).gameObject);
            slotStatus.Add(true);
        }
    }
}
