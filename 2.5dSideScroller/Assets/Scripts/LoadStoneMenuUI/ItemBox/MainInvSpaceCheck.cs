﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainInvSpaceCheck : MonoBehaviour {

    public void CheckMainInvSpace(string itemName)
    {
        Debug.Log("In space check script");
        if (this.GetComponent<AddToMainInv>().itemsList.Count < 10)
        {
            this.GetComponent<AddToMainInv>().AddItem(itemName);
        }        
    }
}
