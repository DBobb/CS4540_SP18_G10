﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class OrganizeTrigger : MonoBehaviour, ISelectHandler {

    public Sprite selectedButton;
    public Sprite unSelectedButton;
    // Update is called once per frame
    public void OnSelect(BaseEventData eventData) {
        this.transform.GetChild(1).GetComponent<Image>().sprite = selectedButton;

    }
    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject != this.gameObject){
            this.transform.GetChild(1).GetComponent<Image>().sprite = unSelectedButton;
        }
    }
}
