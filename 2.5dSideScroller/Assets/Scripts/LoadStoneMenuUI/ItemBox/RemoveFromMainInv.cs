﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 2 hours
// User Story: 51
// Class Purpose: Removes items from the main inventory and adds it to the temporary inventory.
public class RemoveFromMainInv : MonoBehaviour{
    // Variables to remove the items
    int index;
    string itemToRemove;
    bool ableToAdd = false;

    // This function checks if the player presses any button slot, then removes the item that is contained
    // in that button slot. This item is then transferred to the temporary inventory.
    void Update(){
        // If the player presses 'm' on a slot button
        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == this.gameObject){

            // Get the index of the slot pressed
            index = GameObject.Find("ItemBox").GetComponent<MainInventory>().allSlots.FindIndex(x => x == this.gameObject && x.name == this.gameObject.name );

            // Check if that slot has an item to remove
            if (GameObject.Find("ItemBox").GetComponent<MainInventory>().slotStatus[index] == false){
                
                // Find the actual item to remove
                itemToRemove = GameObject.Find("ItemBox").GetComponent<MainInventory>().allSlots[index].transform.GetChild(0).GetComponent<Text>().text;

                // Check if the temporary inventory has rooom to add the item
                ableToAdd = GameObject.Find("PlayerManager").GetComponent<InventorySpaceCheck>().CountStackOccurances(itemToRemove);

                // Call the addItem function of the temporary inventory
                if (ableToAdd == true){
                    GameObject.Find("PlayerManager").GetComponent<AddItem>().AddInventoryItem(itemToRemove);
                }

                // Update slot status and the text of the slot 
                GameObject.Find("ItemBox").GetComponent<MainInventory>().slotStatus[index] = true;
                GameObject.Find("ItemBox").GetComponent<AddToMainInv>().itemsList.Remove(itemToRemove);
                GameObject.Find("ItemBox").GetComponent<MainInventory>().allSlots[index].transform.GetChild(0).GetComponent<Text>().text = "";
                GameObject.Find("ItemBox").GetComponent<MainInventory>().chooseAllSlots[index].transform.GetChild(0).GetComponent<Text>().text = "";
            }
        }
    }
}
