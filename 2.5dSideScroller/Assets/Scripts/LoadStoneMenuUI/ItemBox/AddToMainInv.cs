﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 2 hours
// User Story: 51
// Class Purpose: Moves items from the temporary inventory into the main inventory.
public class AddToMainInv : MonoBehaviour {
    // Variables to move items
    MainInventory inv;

    public List<string> itemsList;

    int index;

    // This function is called at Awake, it creates the list of items in the main inventory.
    void Awake () {
        inv = this.GetComponent<MainInventory>();
        itemsList = new List<string>();
    }
	
    // This function is called when the player is interacting with the temporary inventory in the lode stone menu.
    // It moves individual items from the temporary inventory to the main inventory.
    public void AddItem(string itemName){
        // Find the first open slot
        index = inv.slotStatus.FindIndex(x => x == true);

        // Set that slot to not empty
        inv.slotStatus[index] = false;

        // Add the item to the main inv. list
        itemsList.Add(itemName);

        // Change the text of the slot to the name of the item added
        inv.allSlots[index].transform.GetChild(0).GetComponent<Text>().text = itemName;
        inv.chooseAllSlots[index].transform.GetChild(0).GetComponent<Text>().text = itemName;
    }
}
