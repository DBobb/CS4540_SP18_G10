﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LSInvInteract : MonoBehaviour {
    Inventory inventory;
    TriggerItemBox itemBox;
    AddItem addItemScript;
    RemoveItem rm;

    int index;
    string itemSelected;

    GameObject calcite;
    GameObject cuprite;
    void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
        itemBox = GameObject.Find("LSMenuCanvas").transform.GetChild(1).transform.GetChild(2).GetComponent<TriggerItemBox>();
        addItemScript = GameObject.Find("PlayerManager").GetComponent<AddItem>();
        rm = GameObject.Find("PlayerManager").GetComponent<RemoveItem>();
    }
    void Update()
    {
        if (Input.GetButtonDown("invPressDown") && GameObject.Find("ItemBox").GetComponent<Canvas>().enabled == true)
        {
            index = 0;

            if (inventory.slotStatus[index] == false){
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }

        else if (Input.GetButtonDown("invPressUp") && GameObject.Find("ItemBox").GetComponent<Canvas>().enabled == true)
        {
            index = 2;
            if (inventory.slotStatus[index] == false)
            {
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }
        else if (Input.GetButtonDown("invPressRight") && GameObject.Find("ItemBox").GetComponent<Canvas>().enabled == true)
        {
            index = 3;
            if (inventory.slotStatus[index] == false)
            {
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }
        else if (Input.GetButtonDown("invPressLeft") && GameObject.Find("ItemBox").GetComponent<Canvas>().enabled == true)
        {
            index = 1;
            if (inventory.slotStatus[index] == false)
            {
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }
    }
}
