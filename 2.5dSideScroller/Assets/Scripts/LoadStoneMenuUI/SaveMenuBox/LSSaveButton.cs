﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 2 day 7 hours
// User Story: 22
// Class Purpose: Saves the player's progress when at a lodestone.
public class LSSaveButton : MonoBehaviour{
    // Variables to save the player's progress
    public GameObject saveSuccess;
    public GameObject saveSuccessCopy;
    public float timeNow;
    float timePassed;

    // This function checks if the player presses the save button and saves the game
    void Update(){
        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == GameObject.Find("SaveSlot1")){

            SaveManager.Instance.Save();

            if (SaveManager.Instance.saved == true){
                GameObject.Find("SaveSuccess").GetComponent<Canvas>().enabled = true;
            }
        }
    }
}

