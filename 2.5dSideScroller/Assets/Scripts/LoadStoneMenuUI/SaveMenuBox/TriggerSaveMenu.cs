﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class TriggerSaveMenu : MonoBehaviour, ISelectHandler{
    public GameObject slot1;

    GameObject sidePanel;

    TriggerPointers trigPointers;

    public void OnSelect(BaseEventData eventData){

        GameObject.Find("UIManager").GetComponent<MakeInvisible>().BecomeInvisible();

        sidePanel = GameObject.Find("SidePanel");
        trigPointers = sidePanel.GetComponent<TriggerPointers>();

        trigPointers.InitializePointer("Save");

        GameObject.Find("SaveMenuBox").GetComponent<Canvas>().enabled = true;
        GameObject.Find("SaveSuccess").GetComponent<Canvas>().enabled = false;
    }
}
