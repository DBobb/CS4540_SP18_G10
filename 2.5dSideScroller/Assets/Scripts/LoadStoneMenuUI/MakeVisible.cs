﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeVisible : MonoBehaviour {

    Transform panelTrans;
    int childCount;

    public void BecomeVisible()
    {
        panelTrans = GameObject.Find("PopUpPanel").transform;
        childCount = panelTrans.childCount;

        for (int i = 0; i < childCount; i++)
        {
            panelTrans.GetChild(i).gameObject.GetComponent<Canvas>().enabled = true;
        }
    }
}
