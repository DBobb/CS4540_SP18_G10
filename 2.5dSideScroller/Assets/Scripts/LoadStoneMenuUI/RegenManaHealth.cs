﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenManaHealth : MonoBehaviour {
	
	// Update is called once per frame
	public void RegenManaAndHealth () {
        CharacterManager.Instance.char1.GetComponent<CharacterHealth>().currentHealth = CharacterManager.Instance.char1.GetComponent<CharacterHealth>().startingHealth;
        CharacterManager.Instance.char1.GetComponent<CharacterMana>().curMana = CharacterManager.Instance.char1.GetComponent<CharacterMana>().startingMana;
        CharacterManager.Instance.char2.GetComponent<CharacterHealth>().currentHealth = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth;
        CharacterManager.Instance.char2.GetComponent<CharacterMana>().curMana = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana;
        CharacterManager.Instance.char3.GetComponent<CharacterHealth>().currentHealth = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth;
        CharacterManager.Instance.char3.GetComponent<CharacterMana>().curMana = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana;

        UIManager.Instance.char1HealthSlider.value = CharacterManager.Instance.char1.GetComponent<CharacterHealth>().startingHealth;
        UIManager.Instance.Char1ManaSlider.value = CharacterManager.Instance.char1.GetComponent<CharacterMana>().startingMana;
        UIManager.Instance.char2HealthSlider.value = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth;
        UIManager.Instance.Char2ManaSlider.value = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana;
        UIManager.Instance.char3HealthSlider.value = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth;
        UIManager.Instance.Char3ManaSlider.value = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana;
    }
}
