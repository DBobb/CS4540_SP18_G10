﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 2 hours
// User Story: 40 and 26
// Class Purpose: Called when an enemy dies. Adds soft bismuth amount dropped by the enemy
//                to the player's soft bismuth count. Updates soft bismuth count on the UI.
public class SoftBismuthDrop : MonoBehaviour {

    // Variables to keep track of soft bismuth amounts
    public int playerSoftBismuthAmount = 0;
    public GameObject softBismuthCount;

    // This function is called on start, it sets the default soft bismuth amount
    void Start(){
        softBismuthCount = GameObject.Find("SoftBismuthCount");

        playerSoftBismuthAmount = 100;

        softBismuthCount.GetComponent<Text>().text = playerSoftBismuthAmount.ToString();
    }

    // This function is called when an enemy's health reaches zero, the enemy drops soft bismuth
    // and it is added to the player's soft bismuth amount
    public void DropSoftBismuth (int amount) {
        playerSoftBismuthAmount += amount;

        softBismuthCount.GetComponent<Text>().text = playerSoftBismuthAmount.ToString();
	}

    // This function is called when a player dies, the player loses all soft bismuth
    public void LoseSoftBismuth (){
        playerSoftBismuthAmount = 0;
        softBismuthCount.GetComponent<Text>().text = playerSoftBismuthAmount.ToString();
    }

}
