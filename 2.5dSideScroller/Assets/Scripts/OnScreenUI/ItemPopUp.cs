﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPopUp : MonoBehaviour {

    public GameObject calcitePopUp;
    GameObject calciteCopyPopUp;
    public GameObject cupritePopUp;
    GameObject cupriteCopyPopUp;

    public void DisplayPopUp(string itemCollected)
    {

        if (itemCollected == "Calcite")
        {
            calciteCopyPopUp = (GameObject)Instantiate(calcitePopUp);
            Destroy(calciteCopyPopUp, 2);
        }
        else if (itemCollected == "Cuprite")
        {
            cupriteCopyPopUp = (GameObject)Instantiate(cupritePopUp);
            Destroy(cupriteCopyPopUp, 2);
        }
    }
}

