﻿using UnityEngine;
using System.Collections;


public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;     // The time in seconds between each attack.
    public int attackDamage = 10;               // The amount of health taken away per attack.

    Animator anim;                              // Reference to the animator component.
    GameObject player;                          // Reference to the player GameObject.
    CharacterHealth characterHealth;            // Reference to the player's health.
    CharacterKnockback characterKnockback;      // Reference to the character's knockback script
    EnemyHealth enemyHealth;                    // Reference to this enemy's health.
    GameObject curChar;


    bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
    float timer;                                // Timer for counting up to the next attack.
        
    void Start()
    {
        // Setting up the references.
        //player = GameObject.FindGameObjectWithTag ("PlayerManager");    // Find the player manager
        //pm = player.GetComponent<PlayerManager>();                      // Copy the pm's script
		curChar = CharacterManager.Instance.curChar;                                           // Get the pm's current character

		//int test = CharacterManager.Instance.testPass;

		//curChar = CharacterManager.Instance.GetCurChar();

		characterHealth = curChar.GetComponent <CharacterHealth> ();    // Get components of the character
        characterKnockback = curChar.GetComponent<CharacterKnockback> ();// Get components of the character

        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent <Animator> ();
    }

    void OnTriggerEnter (Collider other)
    {

        Rigidbody otherRB = other.GetComponent<Rigidbody>();

        // If the entering collider is the player...
		if (other.gameObject == curChar && !characterKnockback.beingKnockedBack)
            {
            // ... the player is in range.
            playerInRange = true;
            }
    }
    
    void OnTriggerExit (Collider other)
    {
        // If the exiting collider is the player...
		if (other.gameObject == curChar)
            {
                // ... the player is no longer in range.
                playerInRange = false;
            }
    }

    void Update ()
    {
        curChar = CharacterManager.Instance.curChar;
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // Get the up-to-date player character
		//int test = CharacterManager.Instance.testPass;
		//GameObject curChar = CharacterManager.Instance.curChar;


		characterHealth = curChar.GetComponent<CharacterHealth>();
        characterKnockback = curChar.GetComponent<CharacterKnockback>();

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive... 
        if (timer >= timeBetweenAttacks && playerInRange )//&& enemyHealth.currentHealth > 0)
        {
            // ... attack.
            Attack();
        }

     }


    void Attack()
    {
        // Reset the timer.
        timer = 0f;

        // If the player has health to lose...
		if (characterHealth.currentHealth > 0 && !InvincibilityManager.Instance.iTrigger)
        {
            // ... damage the player.
            characterHealth.TakeDamage (attackDamage);
            characterKnockback.beingKnockedBack = true;

            characterKnockback.TakeKnockback(gameObject.transform.position);

			//CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().sTime = Time.time;

			//InvincibilityManager.Instance.iTrigger = true;



        }
    }
}
