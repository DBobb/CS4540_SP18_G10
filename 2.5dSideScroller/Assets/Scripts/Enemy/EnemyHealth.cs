﻿// Author: Keaton Smith
// Time spent: 3 hours
// User Story: 29
// Class Purpose: This script allows the enemies to have and maintain health. Also contains 
// a public method for taking damage from character objects. Also contains method for death event.
// Has commented out code for future events regarding death animations and audio clips.
using UnityEngine;


    public class EnemyHealth : MonoBehaviour
    {
        public float startingHealth = 100;            // The amount of health the enemy starts the game with.
        public float currentHealth;                   // The current health the enemy has.
        public float sinkSpeed = 2.5f;              // The speed at which the enemy sinks through the floor when dead.
        public int scoreValue = 10;                 // The amount added to the player's score when the enemy dies.
        public AudioClip deathClip;                 // The sound to play when the enemy dies.
        public int softBismuthAmount;

        Animator anim;                              // Reference to the animator.
        AudioSource enemyAudio;                     // Reference to the audio source.
        ParticleSystem hitParticles;                // Reference to the particle system that plays when the enemy is damaged.
        CapsuleCollider capsuleCollider;            // Reference to the capsule collider.
        public bool isDead;                                // Whether the enemy is dead.
        bool isSinking;                             // Whether the enemy has started sinking through the floor.



        void Start()
		{
			//As the game begins, we store our PowerUp function in The Enemy Manager's Delegate.
			//This allows us to essentially set up a trigger for said function outside the realm of this here script here.
			//DrewEnemyManager.Instance.OnPowerUp += PowerUp;


        }
		
		//PowerUp function allows the enemy to increase the amount of damage it can take. Stored in delegate.
		void PowerUp()
		{
			startingHealth += 100;
			currentHealth += 100;
		}


        void Awake ()
        {
            // Setting up the references.
            anim = GetComponent <Animator> ();
            enemyAudio = GetComponent <AudioSource> ();
            hitParticles = GetComponentInChildren <ParticleSystem> ();
            capsuleCollider = GetComponent <CapsuleCollider> ();

            // Setting the current health when the enemy first spawns.
            currentHealth = startingHealth;
        }


        void Update ()
        {
            // If the enemy should be sinking...
            if(isSinking)
            {
                // ... move the enemy down by the sinkSpeed per second.
                transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
            }
        }


        public void TakeDamage (float amount)
        {
            // If the enemy is dead...
            if(isDead)
                // ... no need to take damage so exit the function.
                return;

            // Play the hurt sound effect.
            //enemyAudio.Play ();

            // Reduce the current health by the amount of damage sustained.
            currentHealth -= amount;
            
            // Set the position of the particle system to where the hit was sustained.
           // hitParticles.transform.position = hitPoint;

            // And play the particles.
           // hitParticles.Play();

            // If the current health is less than or equal to zero...
            if(currentHealth <= 0)
            {
                // ... the enemy is dead.
                Death ();
                //GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().DropSoftBismuth(softBismuthAmount);
        }
    }


        void Death ()
        {
            GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().DropSoftBismuth(softBismuthAmount);
            // The enemy is dead.
            isDead = true;

            // Turn the collider into a trigger so shots can pass through it.
            capsuleCollider.isTrigger = true;

            // Tell the animator that the enemy is dead.
            anim.SetTrigger ("Dead");

            // Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
            enemyAudio.clip = deathClip;
            enemyAudio.Play ();
        }


        public void StartSinking ()
    	{ 
            // Find the rigidbody component and make it kinematic (since we use Translate to sink the enemy).
            GetComponent <Rigidbody> ().isKinematic = true;
        	GetComponent<EnemyAttack>().enabled = false;
            // The enemy should no sink.
            isSinking = true;

            // Increase the score by the enemy's score value.
            ScoreManager.score += scoreValue;

            // After 2 seconds destory the enemy.
            Destroy (gameObject, 1f);
        }
    }
