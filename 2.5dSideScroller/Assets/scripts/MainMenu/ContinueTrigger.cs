﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

// Author: Alivia Johnson
// Time spent: 2 hours
// User Story: 4
// Class Purpose: Loads the game when the player clicks the continue button
public class ContinueTrigger : MonoBehaviour{
    // This function loads Scene01 when the continue button is clicked
    public Sprite highlightedSprite;
    public Sprite normalSprite;
    void Update() {
        if (EventSystem.current.currentSelectedGameObject == GameObject.Find("Continue"))
        {
            GameObject.Find("Continue").GetComponent<Image>().sprite = highlightedSprite;
            GameObject.Find("Continue").GetComponent<RectTransform>().localScale = new Vector3(1.2f, 1.2f, 1);
        }
        else if (EventSystem.current.currentSelectedGameObject != GameObject.Find("Continue"))
        {
            GameObject.Find("Continue").GetComponent<Image>().sprite = normalSprite;
            GameObject.Find("Continue").GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }

        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == GameObject.Find("Continue"))
        {
            GameObject.Find("SelectSaveSlot").GetComponent<Canvas>().enabled = true;

            if(SaveManager.Instance.continueGame == true)
            {
                GameObject.Find("SaveSlot").GetComponent<Canvas>().enabled = true;
                Invoke("SetSelected", 0.005f);
 
            }
            else if(SaveManager.Instance.continueGame == false)
            {
                GameObject.Find("SaveSlot").GetComponent<Canvas>().enabled = false;
            }
        }
        if (GameObject.Find("SelectSaveSlot").GetComponent<Canvas>().enabled && Input.GetButtonDown("Cancel"))
        {
            GameObject.Find("SelectSaveSlot").GetComponent<Canvas>().enabled = false;
            EventSystem.current.SetSelectedGameObject(GameObject.Find("Continue"));
        }

        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == GameObject.Find("SaveSlot"))
        {
            SaveManager.Instance.continuePressed = true;
            SaveManager.Instance.continueGame = true;
            GameObject.Find("LoadScene").GetComponent<LoadScene>().Load("Scene03");
        }
    }

    void SetSelected()
    {
        EventSystem.current.SetSelectedGameObject(GameObject.Find("SaveSlot"));
    }
}
