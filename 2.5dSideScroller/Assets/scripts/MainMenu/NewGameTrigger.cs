﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Author: Alivia Johnson
// Time spent: 5 hours
// User Story: 1
// Class Purpose: Loads a new game when the player clicks the new game button
public class NewGameTrigger : MonoBehaviour{
    public Sprite highlightedSprite;
    public Sprite normalSprite;
    bool wasClicked = false;

   // This function loads Scene03 when the new game button is clicked
   void Update(){
        if(EventSystem.current.currentSelectedGameObject == GameObject.Find("NewGame"))
        {
            GameObject.Find("NewGame").GetComponent<Image>().sprite = highlightedSprite;
            GameObject.Find("NewGame").GetComponent<RectTransform>().localScale = new Vector3(1.2f, 1.2f, 1);
        }
        else if(EventSystem.current.currentSelectedGameObject != GameObject.Find("NewGame"))
        {
            GameObject.Find("NewGame").GetComponent<Image>().sprite = normalSprite;
            GameObject.Find("NewGame").GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }

        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == GameObject.Find("NewGame")){

            GameObject.Find("NewGameWarningCanvas").GetComponent<Canvas>().enabled = true;
            Invoke("SelectButton", 0.005f);

        }
        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == GameObject.Find("YesButton"))
        {
            SaveManager.Instance.Delete();
            SaveManager.Instance.continueGame = false;
            GameObject.Find("LoadScene").GetComponent<LoadScene>().Load("Scene03");
        }
        else if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == GameObject.Find("NoButton"))
        {
            GameObject.Find("NewGameWarningCanvas").GetComponent<Canvas>().enabled = false;
            EventSystem.current.SetSelectedGameObject(GameObject.Find("NewGame"));
        }

    }
    void SelectButton()
    {
        EventSystem.current.SetSelectedGameObject(GameObject.Find("YesButton"));
    }
}
