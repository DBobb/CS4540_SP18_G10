﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class OptionsTrigger : MonoBehaviour {
    // This function loads Scene01 when the continue button is clicked
    public Sprite highlightedSprite;
    public Sprite normalSprite;
    void Update () {

        if (EventSystem.current.currentSelectedGameObject == this.gameObject)
        {
            this.gameObject.GetComponent<Image>().sprite = highlightedSprite;
            this.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.2f, 1.2f, 1);
        }
        else if (EventSystem.current.currentSelectedGameObject != this.gameObject)
        {
            this.gameObject.GetComponent<Image>().sprite = normalSprite;
            this.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }

        if (EventSystem.current.currentSelectedGameObject == this.gameObject && Input.GetButtonDown("Confirm"))
        {
            GameObject.Find("OptionCanvas").GetComponent<Canvas>().enabled = true;
            EventSystem.current.SetSelectedGameObject(GameObject.Find("IncreaseButton"));
        }
	}
}
