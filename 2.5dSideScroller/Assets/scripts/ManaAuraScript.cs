﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaAuraScript : MonoBehaviour
{

    // Current character variables
    GameObject player;
    GameObject curChar;

    public ParticleSystem partSys;

    // Use this for initialization
    void Start()
    {
        // Find the PlayerManager
        player = GameObject.FindGameObjectWithTag("PlayerManager");

        // Set pm to an instance of PlayerManager
        //pm = player.GetComponent<PlayerManager>();

        // Set the curChar to the curChar in the PlayerManager
        //curChar = pm.curChar;
		GameObject curChar = CharacterManager.Instance.curChar;
        partSys.enableEmission = false;
        partSys.playbackSpeed = 100;
    }

    // Update is called once per frame
    void Update()

    {/*
        if (pm.curCharManaSlider.value > 0 )//&& Input.GetAxisRaw("UseMana") > 0)

    
		//if (UIManager.Instance.curCharManaSlider.value > 0 && Input.GetAxisRaw("UseMana") > 0)

        {
            partSys.enableEmission = true;
        }
        else
            partSys.enableEmission = false;
            */
    }
}
