﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 5 hours
// User Story: 23
// Class Purpose: Decides if there is enough space for the item
//                passed in to be added to the inventory.
public class InventorySpaceCheck : MonoBehaviour {
    // Variables to check space
    GameObject inv;
    GameObject player;

    Inventory inventory;
    AddItem listCheck;

    int remainingSlots;
    int index2;
    int intCount;
    string stringCount;
    bool ableToAdd = false;
    public int maxStackSize;

    void Start () {
        // Finds the inventory 
        inv = GameObject.FindGameObjectWithTag("Inventory");
        // Copies the Inventory script
        inventory = inv.GetComponent<Inventory>();

        // Finds the player manager
        player = GameObject.FindGameObjectWithTag("PlayerManager");
        // Copies the AddItem script
        listCheck = player.GetComponent<AddItem>();

        // Remaining slots is initialized with the number of slots
        // created
        remainingSlots = inventory.slots;

    }
    
    // Function that counts the number of occurances of the item
    // in the inventory, this will be its stack size
    public bool CountStackOccurances(string itemToAdd){

        index2 = listCheck.itemsList.FindIndex(x => x == itemToAdd);

        stringCount = inventory.allSlotCounts[index2];

        if (stringCount == "") { intCount = 0; }
        else { intCount = System.Convert.ToInt32(stringCount); }

        if (intCount < maxStackSize){
            ableToAdd = true;
        }
        else { ableToAdd = false; }

        if ((ableToAdd == true) && (intCount == 0)){
            remainingSlots--;
        }

        return ableToAdd;
    }
}
