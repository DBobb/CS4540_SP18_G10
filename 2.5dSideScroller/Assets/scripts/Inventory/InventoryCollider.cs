﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 5 hours
// User Story: 23
// Class Purpose: Detects when the current player collides 
//                with an item that can be added to the inventory.
public class InventoryCollider : MonoBehaviour {
    // Variables to detect collision with inventory item
    GameObject player;
    GameObject inv;

    string itemToAdd;
    int remainingSlots;
    int index2;
    int intCount;
    string stringCount;

    AddItem adding;
    Inventory inventory;

    void Start () {
        // Finds the player manager
        player = GameObject.FindGameObjectWithTag("PlayerManager");
       
        // Copies the AddItem script
        adding = player.GetComponent<AddItem>();

        // Finds the inventory 
        inv = GameObject.FindGameObjectWithTag("Inventory");

        // Copies the Inventory script
        inventory = inv.GetComponent<Inventory>();

        
    }

    // Function that decides if the item collided with can be added to the 
    // inventory (based on space and if it is a collectable item)
    void OnTriggerEnter(Collider col){
        // Boolean variable that tells the function if it is able to add the item
        bool ableToAdd = false;
        
        if (col.gameObject.name == "Calcite" || col.gameObject.name == "Cuprite"){
            itemToAdd = col.gameObject.name;
            Destroy(col.gameObject);

            ableToAdd = player.GetComponent<InventorySpaceCheck>().CountStackOccurances(col.gameObject.name);

            if (ableToAdd == true){
                GameObject.Find("UIManager").GetComponent<ItemPopUp>().DisplayPopUp(col.gameObject.name);
                adding.AddInventoryItem(itemToAdd);
            }
        }        
    }
}
