﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 1 day 7 hours
// User Story: 33
// Class Purpose: Detects when the current player collides 
//                with an item that can be added to the inventory.
public class RemoveItem : MonoBehaviour
{

    GameObject pm;
    GameObject inv;
    GameObject curChar;

    CharacterHealth health;
    AddItem add;
    Inventory inventory;
    AddToMainInv itemBox;

    string itemName;
    string itemTag;
    string stringCount;
    int intCount;
    int index;
    int emptyIndex;

    // Use this for initialization
    void Start()
    {
        pm = GameObject.FindGameObjectWithTag("PlayerManager");
        add = pm.GetComponent<AddItem>();
        curChar = CharacterManager.Instance.curChar;

        inv = GameObject.FindGameObjectWithTag("Inventory");
        inventory = inv.GetComponent<Inventory>();
    }

    public void Remove(GameObject itemToRemove)
    {
        curChar = CharacterManager.Instance.curChar;
        health = curChar.GetComponent<CharacterHealth>();

        itemTag = itemToRemove.tag;
        if (itemTag == "CalciteIcon") { itemName = "Calcite"; }
        else if (itemTag == "CupriteIcon") { itemName = "Cuprite"; }

        index = add.itemsList.FindIndex(x => x == itemName);

        stringCount = inventory.allSlotCounts[index];

        if (stringCount == "")
        {
            intCount = 0;
        }
        else
        {
            intCount = System.Convert.ToInt32(stringCount);
        }

        if (intCount > 1)
        {
            intCount -= 1;
            inventory.allSlotCounts[index] = intCount.ToString();
            inventory.gameObjectCounts[index].text = inventory.allSlotCounts[index];

            if (itemName == "Calcite" && GameObject.Find("PlayerManager").GetComponent<InventoryInteract>().isInteractable == true)
            {
                health.AddHealthPotion();
            }
            else if (itemName == "Calcite" && GameObject.Find("PlayerManager").GetComponent<InventoryInteract>().isInteractable == false)
            {
                GameObject.Find("ItemBox").GetComponent<MainInvSpaceCheck>().CheckMainInvSpace(itemName);
            }
            else if (itemName == "Cuprite" && GameObject.Find("PlayerManager").GetComponent<InventoryInteract>().isInteractable == false)
            {
                GameObject.Find("ItemBox").GetComponent<MainInvSpaceCheck>().CheckMainInvSpace(itemName);
            }
        }
        else if (intCount == 1)
        {
            inventory.allSlotCounts[index] = "";
            inventory.gameObjectCounts[index].text = inventory.allSlotCounts[index];
            intCount = 0;

            inventory.slotStatus[index] = true;

            Destroy(itemToRemove);

            if (itemName == "Calcite" && GameObject.Find("PlayerManager").GetComponent<InventoryInteract>().isInteractable == true)
            {
                health.AddHealthPotion();
            }
            else if (itemName == "Calcite" && GameObject.Find("PlayerManager").GetComponent<InventoryInteract>().isInteractable == false)
            {
                GameObject.Find("ItemBox").GetComponent<MainInvSpaceCheck>().CheckMainInvSpace(itemName);
            }
            else if (itemName == "Cuprite" && GameObject.Find("PlayerManager").GetComponent<InventoryInteract>().isInteractable == false)
            {
                GameObject.Find("ItemBox").GetComponent<MainInvSpaceCheck>().CheckMainInvSpace(itemName);
            }
        }
    }
}
