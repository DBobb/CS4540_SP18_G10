﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Author: Alivia Johnson
// Time spent: 15 hours
// User Story: 33
// Class Purpose: Creates an inventory layout automatically based on 
//                certain variables set in the inspector
public class Inventory : MonoBehaviour {

    // Variables that affect the appearance of the inventory layout
    public List<GameObject> allSlots;
    public List<string> allSlotCounts;
    public List<Text> gameObjectCounts;
    public List<bool> slotStatus;

    public int slots;
    int emptySlot;

    void Awake (){
        allSlots = new List<GameObject>();
        allSlotCounts = new List<string>();
        slotStatus = new List<bool>();
        gameObjectCounts = new List<Text>();

        CreateLayout();
	}

    // Function taht creates the inventory layout
    private void CreateLayout(){
        emptySlot = slots;

        allSlots.Add(GameObject.Find("Slot0"));
        allSlots.Add(GameObject.Find("Slot1"));
        allSlots.Add(GameObject.Find("Slot2"));
        allSlots.Add(GameObject.Find("Slot3"));

        gameObjectCounts.Add(GameObject.Find("SlotCount0").GetComponent<Text>());
        gameObjectCounts.Add(GameObject.Find("SlotCount1").GetComponent<Text>());
        gameObjectCounts.Add(GameObject.Find("SlotCount2").GetComponent<Text>());
        gameObjectCounts.Add(GameObject.Find("SlotCount3").GetComponent<Text>());

        allSlotCounts.Add(GameObject.Find("SlotCount0").GetComponent<Text>().text.ToString());
        allSlotCounts.Add(GameObject.Find("SlotCount1").GetComponent<Text>().text.ToString());
        allSlotCounts.Add(GameObject.Find("SlotCount2").GetComponent<Text>().text.ToString());
        allSlotCounts.Add(GameObject.Find("SlotCount3").GetComponent<Text>().text.ToString());

        slotStatus.Add(true);
        slotStatus.Add(true);
        slotStatus.Add(true);
        slotStatus.Add(true);
    }
}
