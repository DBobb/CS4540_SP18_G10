﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryInteract : MonoBehaviour
{
    GameObject calcite;
    GameObject cuprite;
    GameObject inv;

    string itemSelected;
    int index;
    int itemIndex;
    bool buttonPressed = false;
    public bool isInteractable = true;

    AddItem addItemScript;
    RemoveItem rm;
    Inventory inventory;
    DPadButtons dpad;
    void Start()
    {
        inv = GameObject.FindGameObjectWithTag("Inventory");
        inventory = inv.GetComponent<Inventory>();
        dpad = GetComponent<DPadButtons>();
        addItemScript = GameObject.Find("PlayerManager").GetComponent<AddItem>();
        rm = GameObject.Find("PlayerManager").GetComponent<RemoveItem>();
    }
    // Update is called once per frame
    void Update()
    {
        if ((dpad.down && isInteractable == true) || (Input.GetButtonDown("invPressDown") && isInteractable == true))
        {
            index = 0;

            if (inventory.slotStatus[index] == false)
            {
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }

        else if ((dpad.up && isInteractable == true) || Input.GetButtonDown("invPressUp") && isInteractable == true)
        {
            index = 2;
            if (inventory.slotStatus[index] == false)
            {
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }
        else if ((dpad.right && isInteractable == true) ||  Input.GetButtonDown("invPressRight") && isInteractable == true)
        {
            index = 3;
            if (inventory.slotStatus[index] == false)
            {
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }
        else if ((dpad.left && isInteractable == true) || Input.GetButtonDown("invPressLeft") && isInteractable == true)
        {
            index = 1;
            if (inventory.slotStatus[index] == false)
            {
                itemSelected = addItemScript.itemsList[index];

                if (itemSelected == "Calcite")
                {
                    calcite = GameObject.FindGameObjectWithTag("CalciteIcon");
                    rm.Remove(calcite);
                }
                else if (itemSelected == "Cuprite")
                {
                    cuprite = GameObject.FindGameObjectWithTag("CupriteIcon");
                    rm.Remove(cuprite);
                }
            }
        }
    }
}
