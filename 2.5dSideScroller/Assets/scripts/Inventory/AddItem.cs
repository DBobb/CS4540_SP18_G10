﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Author: Alivia Johnson
// Time spent: 10 hours
// User Story: 23
// Class Purpose: Adds the item to the inventory.
public class AddItem : MonoBehaviour
{
    // Variables to add the item
    GameObject slotObject;
    GameObject inv;
    public GameObject calciteSlotImage;
    public GameObject cupriteSlotImage;

    Inventory inventory;

    RectTransform slotRect;

    public int ItemCount = 0;

    public List<string> itemsList;
    public List<GameObject> iconList;

    int index;
    string stringCount;
    int intCount;
    int emptyIndex;

    void Awake()
    {
        // Find the inventory
        inv = GameObject.FindGameObjectWithTag("Inventory");

        // Copies the Inventory script
        inventory = inv.GetComponent<Inventory>();

        itemsList = new List<string>();
        iconList = new List<GameObject>();

        itemsList.Add("Cuprite");
        itemsList.Add("Calcite");
        itemsList.Add("");
        itemsList.Add("");


        iconList.Add(null);
        iconList.Add(null);
        iconList.Add(null);
        iconList.Add(null);
    }

    // Function that adds the item to the inventory
    public void AddInventoryItem(string item)
    {
        index = itemsList.FindIndex(x => x == item);

        stringCount = inventory.allSlotCounts[index];
        if (stringCount == ""){
            intCount = 0;
        }
        else{
            intCount = System.Convert.ToInt32(stringCount);
        }

        if (intCount == 0){
            inventory.slotStatus[index] = false;

            intCount += 1;

            inventory.allSlotCounts[index] = intCount.ToString();
            inventory.gameObjectCounts[index].text = inventory.allSlotCounts[index];

            if (item == "Cuprite"){
                slotObject = (GameObject)Instantiate(cupriteSlotImage);
                iconList[index] = (slotObject);
            }
            else if (item == "Calcite"){
                slotObject = (GameObject)Instantiate(calciteSlotImage);
                iconList[index] = (slotObject);
            }

            slotRect = slotObject.GetComponent<RectTransform>();
            slotRect.transform.SetParent(inventory.allSlots[index].transform);
            slotRect.localPosition = new Vector3(30, -54, 0);
            slotRect.localScale = new Vector3(1,1,1);
        }
        else{
            intCount += 1;
            inventory.allSlotCounts[index] = intCount.ToString();
            inventory.gameObjectCounts[index].text = inventory.allSlotCounts[index];

        }
    }
}


