﻿using UnityEngine;
using System.Collections;

// Author: Drew Bobbitt

// Time spent: 1 hours

// User Story: 27

// Class Purpose: Providing force to arrow object and deleting after given amount of time

public class FireProjectile : MonoBehaviour {

	//Specifies the time between shots
	public float coolDown = 0.15f; 

	//A variable for the game object to be fired
	public GameObject projectile;

	float nextBullet;
	// Use this for initialization
	void Start () {
		nextBullet = 0f;
	
	}
	
	// Update is called once per frame
	void Update () {
		//Shooting Logic
		CharacterMovement myPlayer = transform.root.GetComponent<CharacterMovement> ();

		//Determines direction the bullet fires and the rate at which it fires
		if (Input.GetAxisRaw ("HeavyAttack") > 0 && nextBullet<Time.time) {
			nextBullet = Time.time + coolDown;
			Vector3 rotation;
			if (myPlayer.GetFacing () == 1f)
				rotation = new Vector3 (0, 0, 90);
			else
				rotation = new Vector3 (0, 0, -90);
			
			Instantiate (projectile, transform.position, Quaternion.Euler (rotation));
		}
		//playerController myPlayer = transform.root.GetComponent<playerController> ();
	}
}
