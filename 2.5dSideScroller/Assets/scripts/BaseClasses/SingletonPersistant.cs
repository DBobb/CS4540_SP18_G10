﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Author: Robert Bobbitt
// Time spent: 5 hours
// User Story: N/A
// Class Purpose:
//The following is a base class that is only intended to be inherited from, it allows for the creation of a number of managers that 
//act as singletons. The "Persistant" tacked on the end, refers to the singletons ability to persist between scenes.

public abstract class SingletonPersistant<T> : MonoBehaviour where T : Component
{

	//Used as storage to keep information hidden
	private static T instance; 

	//used to access instance
	public static T Instance {
		get{
			//If our manager doesn't exist we create an object and attach an instance of our manager to it
			//This is a base class so the type of manager we attach is arbitrary.
			if (instance == null) {
				//Just some insurance to ensure that one doesn't already exist and we just couldn't find it.
				instance = FindObjectOfType<T> ();
				//This just says, if we don't have an instance of our persistant manager, then we should make one.
				if (instance == null) {

					var obj = new GameObject();
					obj.hideFlags = HideFlags.HideAndDontSave;
					instance = obj.AddComponent<T> ();
				}


			}

			//we return an "instance" of the specified manager
			return instance;
		}

		set{
			//basic setter function that allows us to manipulate data inside managers.
			instance = value;
		}

	}

	//Note: Should your manager have an Awake function, type it as (proctected override void Awake()) this will ensure both awake functions run properly
	protected virtual void Awake(){

		//Don't Destroy OnLoad Essentially allows us to persist between scenes
		DontDestroyOnLoad (this);
		if (instance == null) {
			instance = this as T; //store as whatever is passed as T
		} else {
			Destroy (gameObject);
		}
	}
}
