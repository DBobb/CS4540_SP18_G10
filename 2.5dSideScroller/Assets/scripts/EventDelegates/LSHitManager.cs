﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Robert Bobbitt
// Time spent: 5 hours
// User Story: N/A
// Class Purpose: Allow a string of events to trigger when loadstone is hit through delegate.

public class LSHitManager : Singleton<LSHitManager> {

	public bool triggerOnHit = false;
	
	// Update is called once per frame
	void Update () {
		
	}


	//Intializing our Delegate
	public delegate void LSDelegate();

	//We make instance of our PowerupDelegate that other objects can access and store functions in.
	public LSDelegate OnHit;

}
