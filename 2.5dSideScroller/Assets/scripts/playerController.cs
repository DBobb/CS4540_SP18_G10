﻿using System;
using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour
{

    // ****** Random Garbage Variables that should be cleaned up later ******
    string guiMessage = "";

    // ****** Movement Variables ******
    public float runSpeed;
    Rigidbody myRB;
    Animator myAnim;
    bool facingRight;


    // ****** Jumping Variables ******
    public bool grounded = false;
    Collider[] groundCollisions;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpForce;

    // ****** State Variables ******
    public enum playerState
    {
        defaultState,
        gotHitState,
        featAttackState
    }
    playerState currentState;
    private float lastStateChange = 0.0f;  //define reference time-variable
    public float hitStunTime;

    // ****************** Begin Methods ******************

    //define method for setting the state of playerState
    void setCurrentState(playerState state)
    {
        currentState = state;
        lastStateChange = Time.time;
    }

    // Use this for initialization
    void Start()
    {
        setCurrentState(playerState.defaultState); //call function to set state to idle
        myRB = GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
        facingRight = true;
    }

    // Use fixed update for physics updates
    // This is our state machine for player
    void FixedUpdate()
    {
        switch (currentState)
        {
            case playerState.defaultState:
                DefaultPlayerOptions();
                break;
            case playerState.gotHitState:
                GotHitPlayerOptions();
                break;
            case playerState.featAttackState:

                break;
        }
    }

    // Update is called once per frame
    void Update()
    {


    }

    // This is the Default State for character Objects
    // All default options should be called in here such as
    // doing normal attacks, 
    // jumping, getting inputs, and moving.
    void DefaultPlayerOptions()
    {
        PlayerJump();

        CheckGrounded();

        GetInputs();


    }

    // This is the State for getting Hit by an enemy.
    // Player should take damage, recieve knockback, and 
    // Have brief invincibilty that lasts for a short time.
    // Also, Controls should be taken away for a moment.
    void GotHitPlayerOptions()
    {

    }


    void PlayerJump()
    {
        if (grounded && Input.GetAxis("Jump") > 0)
        {
            grounded = false;
            myAnim.SetBool("grounded", grounded);
            myRB.AddForce(new Vector3(0, jumpForce, 0));
        }
    }
    void CheckGrounded()
    {
        //Checking for ground collisions
        groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);
        if (groundCollisions.Length > 0)
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
        //Assigning the grounded variable found to the grounded variable in the player animator
        myAnim.SetBool("grounded", grounded);

    }

    //This function simply flips the player model about the z axis
    void FlipModel()
    {
        facingRight = !facingRight; //change our facing boolean to reflect current direction we're facing
        Vector3 modelScale = transform.localScale;
        modelScale.z *= -1;
        transform.localScale = modelScale;
    }


    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 30), guiMessage);
    }

    // This is used for detecting collison with Objects, such as ground or enemies
    void OnCollisionEnter(Collision col)
    {
        guiMessage = col.gameObject.name;

        if (col.gameObject.name == "Enemy")
        {
            //  setCurrentState(playerState.gotHitState);
        }
    }


    void GetInputs()
    {
        float move = Input.GetAxis("Horizontal");
        myAnim.SetFloat("hspeed", Mathf.Abs(move));
        myRB.velocity = new Vector3(move * runSpeed, myRB.velocity.y, 0);

        // Using Inputs and Hspeed to Filp model in animator
        if (move > 0 && !facingRight)
            FlipModel();
        else if (move < 0 && facingRight)
            FlipModel();
    }

    public float GetFacing()
    {
        if (facingRight)
            return 1;
        else
            return -1;
    }

}
