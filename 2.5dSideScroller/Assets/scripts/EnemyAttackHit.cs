﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackHit : MonoBehaviour {

	CharacterHealth characterHealth;            // Reference to the player's health.
	CharacterKnockback characterKnockback;      
	GameObject curChar;
	// Use this for initialization
	void Start () {
		curChar = CharacterManager.Instance.curChar;// Get the pm's current character
		characterHealth = curChar.GetComponent <CharacterHealth> ();// Get components of the character
		characterKnockback = curChar.GetComponent<CharacterKnockback> ();// Get components of the character

		//enemyHealth = GetComponent<EnemyHealth>();
	}
	
	// Update is called once per frame
	void Update () {
		curChar = CharacterManager.Instance.curChar;// Get the pm's current character
		characterHealth = curChar.GetComponent <CharacterHealth> ();// Get components of the character
		characterKnockback = curChar.GetComponent<CharacterKnockback> ();// Get components of the character
	}

	void OnTriggerEnter (Collider other)
	{

		Rigidbody otherRB = other.GetComponent<Rigidbody>();

		// If the entering collider is the player...
		if (other.gameObject == CharacterManager.Instance.curChar && !CharacterManager.Instance.curChar.GetComponent<CharacterKnockback>().beingKnockedBack)
		{
			// ... the player is in range.
			Attack();
		}
	}



	void Attack()
	{
		// Reset the timer.
		//timer = 0f;
		//CharacterManager.Instance.GetComponent<CharacterHealth>().currentHealth
		// If the player has health to lose...
		if ( characterHealth.currentHealth > 0 && !InvincibilityManager.Instance.iTrigger)
		{
			// ... damage the player.
			characterHealth.TakeDamage (10);
			characterKnockback.beingKnockedBack = true;
			characterKnockback.TakeKnockback(gameObject.transform.position);

			CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().sTime = Time.time;

			InvincibilityManager.Instance.iTrigger = true;


		}
	}
}
