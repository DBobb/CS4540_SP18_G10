﻿using UnityEngine;
using System.Collections;

public class DPadButtons : MonoBehaviour
{
    public  bool up;
    public  bool down;
    public  bool left;
    public  bool right;

    float lastX;
    float lastY;

    bool pressed = false;
    void Start()
    {
        up = down = left = right = false;
        lastX = lastY = 0;
    }



    void Update()
    {
        if (!pressed)
        {
            if(Mathf.Abs(Input.GetAxis("DPadX")) == 1 || Mathf.Abs(Input.GetAxis("DPadY")) == 1)
            {
                if (Input.GetAxis("DPadX") == 1 && lastX != 1) { right = true; lastX = 1; } else { right = false; lastX = 0; }
                if (Input.GetAxis("DPadX") == -1 && lastX != -1) { left = true; lastX = -1; } else { left = false; lastX = 0; }
                if (Input.GetAxis("DPadY") == 1 && lastY != 1) { up = true; lastY = 1; } else { up = false; lastY = 0; }
                if (Input.GetAxis("DPadY") == -1 && lastY != -1) { down = true; lastY = -1; } else { down = false; lastY = 0; }

                pressed = true;
            }
        }
        else if(pressed)
        {
            right = false;
            left = false;
            up = false;
            down = false;
            Invoke("ResetPressed", .2f);
        }
        
    }

    void ResetPressed()
    {
        pressed = false;
    }
}