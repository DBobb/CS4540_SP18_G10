﻿using UnityEngine;
using System.Collections;


// Author: Drew Bobbitt

// Time spent: 1 hours

// User Story: 27

// Class Purpose: Providing force to arrow object and deleting after given amount of time


public class ArrowStats : MonoBehaviour
{

    //Intializing variables
    public float speed;
    public float damage;
    public float hitBoxSize;

    float flyTime = 2f;
    float timer = 0;
    Rigidbody myRB;
    Collider myCOL;
    Vector3 origPos;
    public Vector3 direc;
    float angle;
    float side;
    // Use this for initialization
    void Start()
    {
        myRB = GetComponent<Rigidbody>();
        origPos = myRB.position;

        this.transform.localScale *= hitBoxSize;

        if (side > 0)
        {
            if (direc.x != 0 && direc.y != 0) myRB.AddForce(Vector3.right + (direc * speed), ForceMode.Impulse);
            else myRB.AddForce(Vector3.right * speed, ForceMode.Impulse);
            //this.transform.Rotate(Vector3.up, 90 * direc.x);
            this.transform.Rotate(Vector3.forward, angle);
        }
        else
        {
            if (direc.x != 0 && direc.y != 0) myRB.AddForce(Vector3.left + (direc * speed), ForceMode.Impulse);
            else myRB.AddForce(Vector3.left * speed, ForceMode.Impulse);
            //this.transform.Rotate(Vector3.up, 90 * direc.x);
            this.transform.Rotate(Vector3.back, angle );
        }

        
    }

    // Update is called once per frame
    //Essentially deleting the arrow after a given amount of time
    void Update()
    {
            timer += Time.deltaTime;
            if (timer >= flyTime)
            {
            Destroy(gameObject);
                
            }        
    }

    //This function determines whether the arrow collides with a trigger and responds with the appropriate action
    void OnTriggerEnter(Collider other)
    {
        //Collision with ground based entity
       /* if (other.gameObject.layer == 8 || other.gameObject.layer == 14)
        {

            myRB.velocity = Vector3.zero;
            Destroy(gameObject);

        }//Collision with enemy based entity
        else*/ if (other.gameObject.tag == "Enemy")
        {
            other.GetComponent<EnemyHealth>().TakeDamage(damage);
            Destroy(gameObject);
        }

    }

    public void SetStats(float damage, float speed, float size, Vector3 direc, float angle, float side)
    {
        this.damage = damage;
        this.speed = speed;
        this.hitBoxSize = size;
        this.direc = direc;
        this.angle = angle;
        this.side = side;
    }
}
