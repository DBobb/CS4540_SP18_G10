﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Alivia Johnson
// Time spent: 30 minutes
// User Story: 23
// Class Purpose: Spawns Calcite Objects wherever there is a
//                calcite spawn on the map. 
public class SpawnCalcite : MonoBehaviour {
    // Variables to spawn Calcite
    public GameObject calciteObject;
    public GameObject calciteSpawn;
    GameObject newCalciteObject;

	void Start () {
        // Creates a new Calcite object
        newCalciteObject = (GameObject)Instantiate(calciteObject);

        // Names the object "Calcite" so every calcite object is recognized to
        // be the same object by the inventory
        newCalciteObject.name = "Calcite";

        // Sets the calcite position to be the position of the calcite spawn point
        newCalciteObject.transform.position = calciteSpawn.transform.position;

        // Sets the calcite spawn point as the parent to the calcite object
        newCalciteObject.transform.SetParent(calciteSpawn.transform);
    }
}
