﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

// Author: Alivia Johnson
// Time spent: 5 hours
// User Story: 20
// Class Purpose: Detects when the current player collides 
//                with a lodestone, and triggers the lodestone menu.
public class TriggerMenu : MonoBehaviour{
    // Variables for Menu UI
    public GameObject lsMenuUI;
    GameObject popUpPanel;
    GameObject inventory;
    GameObject inventoryPanel;
    GameObject invButton;
    GameObject focusButton;
    GameObject player;

    TriggerItemBox invScript;

    Transform panelTrans;

    public bool gameIsPaused = false;
    int childCount;

    void Update(){
        // If the game is currently paused, and the player presses the 
        // esc key, resume the game
        if (gameIsPaused){
            if (Input.GetButton("Cancel")){
                Resume();
            }
        }
    }

    // When the player collides with the lodestone, pause the game
    void OnTriggerStay(Collider col){
        if (col.gameObject.tag == "LoadStone" && Input.GetButtonDown("interact")){
            GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().HasVisited(col.gameObject);
            GameObject.Find("Lodestones").GetComponent<RegenManaHealth>().RegenManaAndHealth();
            CharacterManager.Instance.char2.GetComponent<CharacterHealth>().isDead = false;
            CharacterManager.Instance.char3.GetComponent<CharacterHealth>().isDead = false;
            Pause();
        }
    }

    // This function resumes the game when called and disables the 
    // lodestone menu
    public void Resume(){


        CharacterManager.Instance.curChar.GetComponent<ItzuliRoll>().enabled = true;
        CharacterManager.Instance.curChar.GetComponent<CharacterJump>().SetCanJump(true);
        Debug.Log("Resuming");


        invButton = GameObject.Find("ItemBoxButton");
        invScript = invButton.GetComponent<TriggerItemBox>();

        if (invScript.hasBeenClicked == true){

            //transferScript.onSideMenu = false;
            inventory = GameObject.Find("InventoryCanvas").transform.GetChild(0).gameObject;

            inventory.GetComponent<Canvas>().enabled = true;
            inventory.transform.SetParent(GameObject.Find("Inventory").transform);
            inventory.transform.localPosition = invScript.oldInvSpot;
        }

        invScript.hasBeenClicked = false;

        GameObject.Find("UIManager").GetComponent<MakeInvisible>().BecomeInvisible();

        for (int i = 0; i < GameObject.Find("LSMenuCanvas").transform.childCount; i++){
            GameObject.Find("LSMenuCanvas").transform.GetChild(i).gameObject.SetActive(false);
        }

        Time.timeScale = 1f;
        gameIsPaused = false;

        player = GameObject.Find("PlayerManager");
        player.GetComponent<InventoryInteract>().isInteractable = true;
    }

    // This function pauses the game when called and enables the
    // lodestone menu
    void Pause(){

        CharacterManager.Instance.curChar.GetComponent<ItzuliRoll>().enabled = false;
        CharacterManager.Instance.curChar.GetComponent<CharacterJump>().SetCanJump(false);

        for (int i = 0; i < GameObject.Find("LSMenuCanvas").transform.childCount; i++){
            GameObject.Find("LSMenuCanvas").transform.GetChild(i).gameObject.SetActive(true);
        }

        GameObject.Find("LevelUpBox").GetComponent<Canvas>().enabled = true;

        focusButton = GameObject.Find("LevelUpButton");
        EventSystem.current.SetSelectedGameObject(focusButton);

        player = GameObject.Find("PlayerManager");
        player.GetComponent<InventoryInteract>().isInteractable = false;

        Time.timeScale = 0f;
        gameIsPaused = true;
    }
}
