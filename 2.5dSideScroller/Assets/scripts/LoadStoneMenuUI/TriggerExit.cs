﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

// Author: Alivia Johnson
// Time spent: 2 hours
// User Story: 7
// Class Purpose: Exits the game from the in-game lodestone menu
public class TriggerExit : MonoBehaviour, ISelectHandler
{

    GameObject sidePanel;
    GameObject panel;
    GameObject itemBox;

    TriggerPointers trigPointers;

    Transform panelTrans;

    int childCount;

    public void OnSelect(BaseEventData eventData)
    {
        GameObject.Find("UIManager").GetComponent<MakeInvisible>().BecomeInvisible();

        sidePanel = GameObject.Find("SidePanel");
        trigPointers = sidePanel.GetComponent<TriggerPointers>();

        trigPointers.InitializePointer("Exit");

        itemBox = GameObject.Find("ItemBoxButton");
        itemBox.GetComponent<TriggerItemBox>().onSideMenu = false;

        GameObject.Find("ExitBox").GetComponent<Canvas>().enabled = true;
    }

    void Update()
    {
        if (Input.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject == GameObject.Find("ExitBox").transform.GetChild(0).transform.GetChild(1).gameObject)
        {
            Time.timeScale = 1f;
            GameObject.Find("LoadScene").GetComponent<LoadMainMenu>().Load();
        }
    }
}
