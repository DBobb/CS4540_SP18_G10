﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPointers : MonoBehaviour {
    public GameObject levelPointer;
    public GameObject itemBoxPointer;
    public GameObject savePointer;
    public GameObject warpPointer;
    public GameObject convertPointer;
    public GameObject exitPointer;
    GameObject[] pointerArray;

    void Awake(){
        pointerArray = new GameObject[6] { levelPointer, itemBoxPointer,
                                           savePointer, warpPointer,
                                           convertPointer, exitPointer};
    }

    public void InitializePointer (string button) {
        for (int i = 0; i < pointerArray.Length; i++){
            pointerArray[i].gameObject.SetActive(false);
        }

        if (button == "LevelUp"){
            pointerArray[0].gameObject.SetActive(true);
        }
        else if (button == "ItemBox"){
            pointerArray[1].gameObject.SetActive(true);
        }
        else if (button == "Save"){
            pointerArray[2].gameObject.SetActive(true);
        }
        else if (button == "Warp"){
            pointerArray[3].gameObject.SetActive(true);
        }
        else if (button == "ConvertToBismuth"){
            pointerArray[4].gameObject.SetActive(true);

        }
        else if (button == "Exit"){
            pointerArray[5].gameObject.SetActive(true);
        }
    }
}
