﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Keaton Smith
// Purpose: This script is to be attached onto Enemy Spawner Manager objects and uses the camera's collision box to trigger enemy spawners.
// This will not work correctly unless there is a SpawnEnemy object within the scene, attached as this object's child
// UserStory: 41
// Time: 3 hours
public class SpawnEnemyManager : MonoBehaviour {

    List<GameObject> allSpawnersInScene = new List<GameObject>();
    GameObject curSpawner;
    int i = 0;
    // Use this for initialization
    void Start () {

        int children = transform.childCount;

        // This adds all of the SpawnEnemy child objects into an array
        for (int j = 0; j < children; ++j)
        {
            allSpawnersInScene.Add((this.transform.GetChild(j).gameObject));
        }
    }
	
	// Update is called once per frame
	void Update () {
        // If the player saves, this flips the saveReset value in all of the spawnEnemy objects. After flipping 
        // all of the spawner's saveReset values, it sets the save value of the save Manager back to false

		if(SaveManager.Instance.saved )
        {
            if( i < allSpawnersInScene.Capacity)
            {
                curSpawner = allSpawnersInScene[i];
                curSpawner.GetComponent<SpawnEnemy>().saveReset = true;
                i++;
            }

            if (i >= allSpawnersInScene.Capacity)
            {
                SaveManager.Instance.saved = false;
                i = 0;
            }
        }
	}
 
}
