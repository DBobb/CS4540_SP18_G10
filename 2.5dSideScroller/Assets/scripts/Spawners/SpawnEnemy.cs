﻿// Author: Keaton Smith
// Purpose: This script is to be attached onto Enemy Spawner objects and uses the camera's collision box to trigger enemy spawners.
// This will not work correctly unless there is a SpawnEnemyManager object within the scene as well. this must be a child of the manager.
// UserStory: 41
// Time: 2 hours
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{

    public GameObject[] enemyToSpawn;
    public bool saveReset = false;

    public GameObject currentEnemyInstance;
    EnemyHealth enemyHealth;
    int curEnemyIndex = 0;

    // Use this for initialization
    void Start()
    {
        currentEnemyInstance = Instantiate(enemyToSpawn[curEnemyIndex], this.transform.position, this.transform.rotation);
        currentEnemyInstance.SetActive(false);
        enemyHealth = currentEnemyInstance.GetComponent<EnemyHealth>();
    }

    // Update is called once per frame
    void Update()
	{
		
        if (saveReset && !enemyHealth.isDead) saveReset = false;
        if (enemyHealth.isDead && saveReset)
        {
            if (curEnemyIndex + 1 < enemyToSpawn.Length) // This increases the difficulty of the enemy, going further up the enemy array with each save the player makes
            {
                curEnemyIndex += 1;
                currentEnemyInstance = Instantiate(this.enemyToSpawn[curEnemyIndex], this.transform.position, this.transform.rotation);
                currentEnemyInstance.SetActive(false);

                enemyHealth = currentEnemyInstance.GetComponent<EnemyHealth>();
                saveReset = false;
            }
            else // This loops at whatever the last enemy in the array was. This would ideally be the most difficult enemy on this spawner
            {
                currentEnemyInstance = Instantiate(this.enemyToSpawn[curEnemyIndex], this.transform.position, this.transform.rotation);
                currentEnemyInstance.SetActive(false);

                enemyHealth = currentEnemyInstance.GetComponent<EnemyHealth>();
                saveReset = false;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "MainCamera" && !enemyHealth.isDead)
        {
            if(currentEnemyInstance.transform.position != this.transform.position && currentEnemyInstance.activeSelf == false)
            {
                currentEnemyInstance.transform.position = this.transform.position;
            }
            currentEnemyInstance.SetActive(true);
        }
    }

}
