﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YaBoi : Enemy {

	// Use this for initialization
	void Start () {

		health = 200;
		speed = 10;
		strength = 50;

		terminalSpeed = speed / 10;
		initialSpeed = (speed / 10) / 2;
		acceleration = (speed / 10) / 4;

		//Get animator here later
		GameObject player = CharacterManager.Instance.curChar;
	}

	public override HashSet<KeyValuePair<string,object>> createGoalState(){
		HashSet<KeyValuePair<string,object>> goal = new HashSet<KeyValuePair<string, object>> ();
		goal.Add (new KeyValuePair<string,object> ("damagePlayer", true));
		goal.Add (new KeyValuePair<string,object> ("stayAlive", true));
		return goal;
	}
}
