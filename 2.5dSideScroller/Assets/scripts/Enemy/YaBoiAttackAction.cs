﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YaBoiAttackAction : GOAPAction {

	private bool attacked = false;

	public YaBoiAttackAction(){
		addEffect ("damagePlayer", true);
		cost = 100f;
	}

	public override void reset() {
		attacked = false;
		target = null;
	}

	public override bool isDone() {
		return attacked;
	}

	public override bool requiresInRange(){
		return true;
	}

	public override bool checkProceduralPrecondition(GameObject agent){
		target = CharacterManager.Instance.curChar;
		return target != null;
	}

	public override bool perform(GameObject agent){
		YaBoi currBoi = agent.GetComponent<YaBoi> ();

		int damage = currBoi.strength;

		attacked = true;
		return true;
		//Give damage to the player or whatever.
	}
}
