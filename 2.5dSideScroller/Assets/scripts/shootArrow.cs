﻿using UnityEngine;
using System.Collections;


// Author: Drew Bobbitt

// Time spent: 1 hours

// User Story: 27

// Class Purpose: Providing force to arrow object and deleting after given amount of time


public class ShootArrow : MonoBehaviour {

	//Intializing variables
	public float speed;
	public float damage;
    public float hitBoxSize;

    float flyTime = 5f;
    float timer = 0;
	Rigidbody myRB;

	// Use this for initialization
	void Start () {
		myRB = GetComponentInParent<Rigidbody> ();

		//Applying a force to the rigidbody attached to the arrow and ensuring it's fired in the right direction
		if (transform.rotation.z > 0)
			myRB.AddForce (Vector3.right * speed, ForceMode.Impulse);
		else
			myRB.AddForce (Vector3.right * -speed, ForceMode.Impulse);
	}
	
	// Update is called once per frame
	//Essentially deleting the arrow after a given amount of time
	void Update () {
        timer += Time.deltaTime;
        if (timer >= flyTime)
        {
			Destroy (gameObject);

        }
	}

	//This function determines whether the arrow collides with a trigger and responds with the appropriate action
	void OnTriggerEnter(Collider other) {
		//Collision with ground based entity
		if (other.gameObject.layer == LayerMask.NameToLayer ("ground")) {
			
			myRB.velocity = Vector3.zero;
			Destroy (gameObject);

		}//Collision with enemy based entity
        else if(other.gameObject.tag == "Enemy")
        {
            other.GetComponent<EnemyHealth>().TakeDamage(damage);
            Destroy(gameObject);
        }
		
	}

}
