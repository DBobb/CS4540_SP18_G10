﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*NOTE: Any agent that wants to utilize GOAP must implement
the following interface. It provides world info to the GOAP 
planner so it can well...plan. It also provides an interface 
to give feedback to the agent and report success or failure.*/

public interface IGOAP {

	//The starting state, supply states needed for actions to run.
	HashSet<KeyValuePair<string,object>> getWorldState();

	//give the planner a new goal so it can formulate a plan.
	HashSet<KeyValuePair<string,object>> createGoalState();

	//No sequence of actions could be found that achieve the goal. Try another one.
	void planFailed(HashSet<KeyValuePair<string,object>> failedGoal);

	//We found a plan that could achieve the goal.
	void planFound (HashSet<KeyValuePair<string,object>> goal, Queue<GOAPAction> actions);

	//We did it man! (The goal was reached, the plan was a success).
	void actionsFinished();

	//One of the actions caused the plan to fail (ABORT FISSION MAILED).
	void planAborted (GOAPAction aborter);

	//To be called during update. The agent will move towards the specified target.

	bool moveAgent(GOAPAction nextAction);
}
