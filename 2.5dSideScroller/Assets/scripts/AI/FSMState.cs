﻿using System.Collections;
using UnityEngine;

public interface FSMState {

	void Update(FSMState fsm, GameObject obj);
}

