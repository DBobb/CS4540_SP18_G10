﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GOAPAction : MonoBehaviour {

	private HashSet<KeyValuePair<string,object>> preconditions;
	private HashSet<KeyValuePair<string,object>> effects;

	private bool inRange = false;

	/*The cost of performing the action. VERY IMPORTANT as the weight is what
	 * determines what actions will be chosen*/
	public float cost = 1f;

	/*This is the object the action will be tied to. This can be null*/
	public GameObject target;

	public GOAPAction() {
		preconditions = new HashSet<KeyValuePair<string, object>> ();
		effects = new HashSet<KeyValuePair<string, object>> ();
	}

	public void doReset() {
		inRange = false;
		target = null;
		reset ();
	}

	//Used to reset any variables that need to be reset before planning starts again
	public abstract void reset();

	//Is the action complete?
	public abstract bool isDone();

	//Check if we can run the action. Not every action will need this, but some might.
	public abstract bool checkProceduralPrecondition(GameObject agent);

	/*Run the action.
	 * It'll return true if the action is successful or false otherwise
	 * If it doesn't run, the action will be cleared and the goal can't be reached.*/

	public abstract bool perform(GameObject agent);

	//Do we need to be in range to perform this action?
	public abstract bool requiresInRange();

	/*Are we in range of the target?
	 * The MoveTo state will set this and it gets reset each time this action is performed. */

	public bool isInRange() {
		return inRange;
	}

	public void setInRange(bool inRange) {
		this.inRange = inRange;
	}

	public void addPrecondition(string key, object value) {
		preconditions.Add (new KeyValuePair<string,object> (key, value));
	}

	public void removePrecondition(string key) {
		KeyValuePair<string, object> remove = default(KeyValuePair<string,object>);
		foreach (KeyValuePair<string, object> kvp in preconditions) {
			if (kvp.Key.Equals (key))
				remove = kvp;
		}
		if ( !default(KeyValuePair<string,object>).Equals(remove) )
			preconditions.Remove (remove);
	}
			public void addEffect(string key, object value) {
				effects.Add (new KeyValuePair<string, object>(key, value) );
			}


			public void removeEffect(string key) {
				KeyValuePair<string, object> remove = default(KeyValuePair<string,object>);
				foreach (KeyValuePair<string, object> kvp in effects) {
					if (kvp.Key.Equals (key))
						remove = kvp;
				}
				if ( !default(KeyValuePair<string,object>).Equals(remove) )
					effects.Remove (remove);
			}


			public HashSet<KeyValuePair<string, object>> Preconditions {
				get {
					return preconditions;
				}
			}

			public HashSet<KeyValuePair<string, object>> Effects {
				get {
					return effects;
				}
			}
}
