﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SliderImageTransform : MonoBehaviour {

    GameObject player;
    GameObject char1, char2, char3;
    Slider char1hs, char2hs, char3hs;
    Slider char1ms, char2ms, char3ms;

    public Sprite transparentHealth;
    public Sprite transparentMana;

    public Sprite normalHealth;
    public Sprite normalMana;

    // Use this for initialization
    void Awake () {
        //player = GameObject.FindGameObjectWithTag("PlayerManager");
        //pm = player.GetComponent<PlayerManager>();

        char1 = CharacterManager.Instance.char1;
		char2 = CharacterManager.Instance.char2;
		char3 = CharacterManager.Instance.char3;

		char1hs = UIManager.Instance.char1HealthSlider;
		char2hs = UIManager.Instance.char2HealthSlider;
		char3hs = UIManager.Instance.char3HealthSlider;
		char1ms = UIManager.Instance.Char1ManaSlider;
		char2ms = UIManager.Instance.Char2ManaSlider;
		char3ms = UIManager.Instance.Char3ManaSlider;

        char1hs.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = normalHealth;
        char1ms.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = normalMana;

        char2hs.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = transparentHealth;
        char2ms.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = transparentMana;

        char3hs.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = transparentHealth;
        char3ms.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = transparentMana;

    }
	public void RevertImage(Slider curCharHealthSlider, Slider curCharManaSlider)
    {
        curCharHealthSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = transparentHealth;
        curCharManaSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = transparentMana;

    }

    public void UpdateImage(Slider curCharHealthSlider, Slider curCharManaSlider)
    {
        curCharHealthSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = normalHealth;
        curCharManaSlider.transform.GetChild(1).GetChild(0).GetComponent<Image>().sprite = normalMana;
    }
}
