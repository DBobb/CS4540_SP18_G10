﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLightAttackAir : MonoBehaviour {

    string attackName = "LightAttackAir";           // Light or  Heavy 
                                                        // ***IMPORTANT*** This name is case sensitive and must match the 
                                                        // input name, animation trigger name, and animation name

    public float damage;                // Amount of damage this attack deals to enemies

    public float startActiveTime;       // A time when the hitbox object first become active during the animation
    //public float endActiveTime;         // A time when the hitbox object becomes inactive during the animation

    public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn

    float startTime = 0f;               // Time tracker to note beginning of attack
    bool isAttacking = false;           // Trigger variable

    Animator animator;                  // PlayerAC
    AnimationClip airAttackAnimation;      // Specific attack clip pulled from PlayerAC

    CharacterMovement characterMovement;// Used for disabling character movement during attack
    CharacterJump characterJump;        // Used for disabling character jumping during attack
    CharacterAttackManagement attackManagement;
    public ParticleSystem particleSystem;

    bool approvalFromManager = false; // this is tied tothe enemy attack manager

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        characterMovement = GetComponent<CharacterMovement>();
        characterJump = GetComponent<CharacterJump>();
        attackManagement = GetComponent<CharacterAttackManagement>();

        airAttackAnimation = GetAnimationClipFromAnimatorByName(animator, attackName);

        attackBox.enabled = false; // Disable the hitbox upon spawn
        attackBox.GetComponent<AttackHit>().damage = this.damage;

        startActiveTime = startActiveTime * airAttackAnimation.length;
        //endActiveTime = endActiveTime * groundAttackAnimation.length;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (approvalFromManager)
        {
            AirAttack();
        }
        else if (isAttacking && !approvalFromManager)
        {
            animator.SetBool(attackName, false);
            attackBox.enabled = false;

            particleSystem.Stop();

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);
            isAttacking = false;
        }
    }
    void AirAttack()
    {
        // Conditions Required to begin attacking
        if (!isAttacking)
        {
            startTime = Time.time; // Get start time of when attack began
            animator.SetBool(attackName, true);
            isAttacking = true;
        }
        // Code to perform if character is attacking
        if (isAttacking == true)
        {
           
            // if we are at the active attacking times
            if (Time.time - startTime >= startActiveTime)
            {
                particleSystem.Play();
                attackBox.enabled = true;
            }
            // if we have exceeded the attacking times

            // Execute this after attack animation finishes
            if (GetComponent<CharacterGrounded>().grounded) //(Time.time - startTime >= groundAttackAnimation.length)
            {

                animator.SetBool(attackName, false);
                attackBox.enabled = false;

                particleSystem.Stop();

                approvalFromManager = false;
                attackManagement.SetIsAttacking(false);
                isAttacking = false;

            }
        }

    }

    // This is a function made to search for an animation clip in an animator by name.
    // We use it to find attack animations (LightAttack, HeavyAttack, etc)
    internal static AnimationClip GetAnimationClipFromAnimatorByName(Animator animator, string name)
    {
        //can't get data if no animator
        if (animator == null)
            return null;

        //favor for above foreach due to performance issues
        for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
        {
            if (animator.runtimeAnimatorController.animationClips[i].name == name)
            {
                return animator.runtimeAnimatorController.animationClips[i];
            }
        }

        Debug.LogError("Animation clip: " + name + " not found");
        return null;
    }

    public void SetManagerApproval(bool result)
    {
        approvalFromManager = result;
    }
}
