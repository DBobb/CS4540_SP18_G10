﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmeliaHeavyAttack : MonoBehaviour {

    public GameObject ameliaHandArrow;
    public GameObject ameliaReleaseArrow;
    public ParticleSystem chargingPartSys;
    public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn
    // Min and max values that player can have based on how long they hold the heavy button;
    public float maxDamage;
    public float minDamage;
    public float maxManaCost;
    public float minManaCost;
    public float maxSpeed;
    public float minSpeed;
    public float maxHitBoxSize;
    public float minHitBoxSize;
    public float growthRate;
    // Current representation of the Damage, Size, Mana Cost, and speed of the arrow
    float curDamage = 0;
    float curManaCost = 0;
    float curSpeed = 0;
    float curSize = 0;
    float beginningPartSize;
    Animator animator;                  // PlayerAC
    CharacterMovement characterMovement;// Used for disabling character movement during attack
    CharacterJump characterJump;        // Used for disabling character jumping during attack
    CharacterAttackManagement attackManagement;
    CharacterMana characterMana;
    Rigidbody amelia;
    public bool activated;
    bool arrowReleased = false;
    bool isAttacking = false;
    float startTime;
    public float startActiveTime;
    public float endActiveTime;
    float totalTime;

    // Use this for initialization
    void Start () {
        attackManagement = GetComponentInParent<CharacterAttackManagement>();

        animator = GetComponentInParent<Animator>();
        characterMovement = GetComponentInParent<CharacterMovement>();
        characterJump = GetComponentInParent<CharacterJump>();
        amelia = GetComponentInParent<Rigidbody>();
        characterMana = GetComponentInParent<CharacterMana>();
        attackBox.GetComponent<AttackHit>().damage = this.curDamage;
        activated = true;
        startActiveTime = startActiveTime * totalTime;
        endActiveTime = endActiveTime * totalTime;
        beginningPartSize = chargingPartSys.startSize;
    }
	
	// Update is called once per frame
	void Update () {
        if (activated)
        {
            GroundHeavyAttack();
        }
    }


    void GroundHeavyAttack()
    {
        Vector3 arrowDirec = new Vector3(0, 0, 0);
        float angle = 0; ;
        // Conditions Required to begin attacking
        if (!isAttacking)
        {
            startTime = Time.time; // Get start time of when attack began
            isAttacking = true;
            activated = true;
            arrowReleased = false;
            curDamage = minDamage;
            curManaCost = minManaCost;
            curSize = minHitBoxSize;
            curSpeed = minSpeed;
            ameliaHandArrow.SetActive(true);
            animator.SetBool("HeavyAttack", true);
            chargingPartSys.Play();

        }
        // Code to perform if character is attacking
        else if (isAttacking == true && arrowReleased == false)
        {
            chargingPartSys.Play();

            if ((Input.GetAxisRaw("Horizontal") == 1 && Input.GetAxisRaw("Vertical") >= 1) || (Input.GetAxisRaw("Horizontal") == -1 && Input.GetAxisRaw("Vertical") >= 1 ) || Input.GetAxisRaw("Vertical") >= 1)   // up and right or up and left
            {
                animator.SetBool("AimUp", true);

                animator.SetBool("AimLevel", false);
                animator.SetBool("AimDown", false);

                arrowDirec = new Vector3(characterMovement.GetFacing() * 1, 1);
                angle = 45;
            }
            else if((Input.GetAxisRaw("Horizontal") == 1 && Input.GetAxisRaw("Vertical") <= -1 )|| (Input.GetAxisRaw("Horizontal") == -1 && Input.GetAxisRaw("Vertical") <= -1) || Input.GetAxisRaw("Vertical") <= -1)// down and right or down and left
            {
                animator.SetBool("AimDown", true);

                animator.SetBool("AimLevel", false);
                animator.SetBool("AimUp", false);

                arrowDirec = new Vector3(characterMovement.GetFacing() * 1, -1);
                angle = -45;
            }
            else if(Input.GetAxisRaw("Horizontal") == 0 || Input.GetAxisRaw("Vertical") == 0) //neutral
            {
                animator.SetBool("AimLevel", true);

                animator.SetBool("AimDown", false);
                animator.SetBool("AimUp", false);

                arrowDirec = new Vector3(0, 0);
                angle = 0;
            }
            chargingPartSys.startSize = (curManaCost/15);
            if(Input.GetButton("HeavyAttack") && curManaCost <= characterMana.curMana)
            {
                if (curDamage < maxDamage) curDamage += curDamage * growthRate;
                if (curManaCost < maxManaCost) curManaCost += curManaCost * growthRate;
                if (curSize < maxHitBoxSize) curSize += curSize * growthRate;
                if (curSpeed < maxSpeed) curSpeed += curSpeed * growthRate;
            }
            // Execute this after attack animation finishes
            else if (Input.GetButtonUp("HeavyAttack") )
            {
                Vector3 rotation;
                if (characterMovement.GetFacing() == 1f)
                    rotation = new Vector3(0, 0, 90);
                else
                    rotation = new Vector3(0, 0, -90);
                float side = characterMovement.GetFacing();

                GameObject arrow =  Instantiate(ameliaReleaseArrow, amelia.transform.position+new Vector3(0,2f,0), Quaternion.Euler(rotation));
                arrow.GetComponent<ArrowStats>().SetStats(curDamage, curSpeed, curSize, arrowDirec, angle, side);
                arrow.GetComponent<Rigidbody>().velocity = arrowDirec;
                arrow.SetActive(true);
                CharacterManager.Instance.curChar.GetComponent<CharacterMana>().UseMana(curManaCost);
                arrowReleased = true;

                animator.SetBool("HeavyFire", true);
            }
        }
        else if(isAttacking == true && arrowReleased == true)
        {
            animator.SetBool("HeavyAttack", false);

            chargingPartSys.Stop();
            chargingPartSys.startSize = beginningPartSize;
            isAttacking = false;
            arrowReleased = false;
            activated = false;
            ameliaHandArrow.SetActive(false);
            GetComponentInParent<CharacterHeavyAttack>().SetIsAttacking(false);
            this.gameObject.SetActive(false);
        }

    }
    public void SetValues(float damage)
    {
        float mmaxDamage = damage;
    }

}
