﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

// Author: Alivia Johnson
// Time spent: 10 hours
// User Story: 2
// Class Purpose: Maintains the current character's mana value.
//                Regenerates the non active players' mana values as well. 
public class CharacterMana : MonoBehaviour {

    // Variables that affect the mana
    public int startingMana = 100;
    public float curMana;
    public float regenMana = 0.05f;

    // Current character variables
    GameObject curChar;

    // Use this for initialization
    void Awake () {
        curMana = startingMana;

		curChar = CharacterManager.Instance.curChar;
    }
	
	// Update is called once per frame
	void Update () {
        curMana = UIManager.Instance.curCharManaSlider.value;
        curChar = CharacterManager.Instance.curChar;
    }

    // Function that uses the current character's mana value
    public void UseMana(float reduceMana)
    {
        // If the player presses the 'm' button, reduce mana's slider value

		if (UIManager.Instance.curCharManaSlider.value > 0)
        {
            
			UIManager.Instance.curCharManaSlider.value -= reduceMana;
            curMana = UIManager.Instance.curCharManaSlider.value;
        }
    }

    // Function that regenerates the nonactive players' mana
  
}
