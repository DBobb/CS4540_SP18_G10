﻿// Author: Keaton Smith
// Time spent: 8 hours
// User Story: 31
// Class Purpose: This script allows the character objects to use feat attacks that require use of mana. It spawns a collider that has
// been placed on the character object. 
// Important to note that attackName is used for animation, animation trigger, and input name
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItzuliHeavyAttack : MonoBehaviour
{
    public GameObject itzuliCrystals;
    public float crystalYScale = 0.01f;
    public float crystalXScale = 0.0f;
    public float maxCrystalYSize = 2;
    public float maxCrystalXSize = 1;

    public float damage;                // Amount of damage this attack deals to enemies

    public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn

    Animator animator;                  // PlayerAC
    CharacterMovement characterMovement;// Used for disabling character movement during attack
    CharacterJump characterJump;        // Used for disabling character jumping during attack
    CharacterAttackManagement attackManagement;

    bool isAttacking = false;
    float startTime;
    public float startActiveTime;
    public float endActiveTime;
    float totalTime;

    Vector3 origCrystalScale;

    // Use this for initialization
    void Start()
    {
        attackManagement = GetComponentInParent<CharacterAttackManagement>();

        animator = GetComponent<Animator>();
        characterMovement = GetComponent<CharacterMovement>();
        characterJump = GetComponent<CharacterJump>();

        attackBox.GetComponent<AttackHit>().damage = this.damage;


        itzuliCrystals.SetActive(false);
        origCrystalScale = itzuliCrystals.transform.localScale;
        startActiveTime = startActiveTime * totalTime;
        endActiveTime = endActiveTime * totalTime;

    }

    // Update is called once per frame
    void Update()
    {
        if (!this.isActiveAndEnabled)
        {
            this.itzuliCrystals.SetActive(false);
        }
        else if(this.isActiveAndEnabled)
        {
            GroundHeavyAttack();
        }
    }

    void GroundHeavyAttack()
    {
        // Conditions Required to begin attacking
        if (!isAttacking)
        {
            startTime = Time.time; // Get start time of when attack began
            isAttacking = true;
        }
        // Code to perform if character is attacking
        if (isAttacking == true)
        {
            // Execute this while the attack animation lasts
            if (Time.time - startTime < totalTime)
            {
                // if we are at the active attacking times
                if (Time.time - startTime >= startActiveTime && Time.time - startTime < endActiveTime)
                {
                    itzuliCrystals.SetActive(true);
                    if(itzuliCrystals.transform.localScale.x < maxCrystalXSize)
                    {
                        itzuliCrystals.transform.localScale += new Vector3(crystalXScale, 0, 0);
                    }
                    if(itzuliCrystals.transform.localScale.y < maxCrystalYSize)
                    {
                        itzuliCrystals.transform.localScale += new Vector3(0, crystalYScale, 0);

                    }
                    attackBox.enabled = true;
                }
                // if we have exceeded the attacking times
                else if (Time.time - startTime > endActiveTime)
                {
                    //itzuliCrystals.transform.localScale = origCrystalScale;

                    itzuliCrystals.SetActive(false);
                    attackBox.enabled = false;
                }
            }
            // Execute this after attack animation finishes
            else if (Time.time - startTime >= totalTime)
            {
                isAttacking = false;
                attackBox.enabled = false;
                itzuliCrystals.transform.localScale = origCrystalScale;

                itzuliCrystals.SetActive(false);

            }
        }


    }
    public void SetValues(float total, float damage)
    { 
        totalTime = total;
        this.damage = damage;
    }
}
