﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItzuliRoll : MonoBehaviour {

     float distPart1 = 5;
     float distTeleport = 0.5f;
     float distPart2 = 0.1f;
     float speedPart1 = 0.4f;
     float speedPart2 = 1f;
    Animator anim;
    bool teleport1 = false;
    AnimationClip tp1Animation;
    AnimationClip tp2Animation;
    float time;
    Rigidbody myRB;
    CharacterMovement myCM;
    CharacterJump myCJ;
    CharacterAttackManagement cam;
    float distCtr = 0;

	float prevActiveTime;
	public float rollTime = 1.0f;
    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        tp1Animation = GetAnimationClipFromAnimatorByName(anim, "Teleport1"); // Pull out the teleport1 animation clip from character
        tp2Animation = GetAnimationClipFromAnimatorByName(anim, "Teleport2"); // Pull out the teleport2 animation clip from characte
        myRB = GetComponent<Rigidbody>();
        myCM = GetComponent<CharacterMovement>();
        myCJ = GetComponent<CharacterJump>();

        cam = GetComponent<CharacterAttackManagement>();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Roll") && !teleport1 && GetComponent<CharacterGrounded>().grounded && !cam.isAttacking)
        {
            distCtr = 0;
			prevActiveTime = CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().timeActive;
			CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().timeActive = 1.0f;
			CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().sTime = Time.time;
			InvincibilityManager.Instance.iTrigger = true;
            time = Time.time;
            teleport1 = true;
        }
	}

    void FixedUpdate()
    {
        if (teleport1)
        {
            DodgeRoll();
        }
    }

    void DodgeRoll()
    {
        if(Time.time - time >=  (tp1Animation.length + tp2Animation.length) /5)
        {
            anim.SetBool("Teleport1", false);
            anim.SetBool("Teleport2", false);
            anim.speed = 1;
            teleport1 = false;
			CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().timeActive = prevActiveTime;
			InvincibilityManager.Instance.iTrigger = false;
			InvincibilityManager.Instance.intangible = false;
            CharacterManager.Instance.curChar.layer = 13; 
            myCM.enabled = true;
            myCJ.SetCanJump(true);
        }
        else
        {
            myCM.enabled = false;
            myCJ.SetCanJump(false);
            if(distCtr < distPart1)
            {
                myRB.transform.position += new Vector3(speedPart1 * myCM.GetFacing(), 0, 0);
                distCtr += speedPart1;
                anim.speed = 2;//1 + (speedPart1*2);

            }
            anim.SetBool("Teleport1", true);
            anim.SetBool("Teleport2", true);
        }


    }

    // This is a function made to search for an animation clip in an animator by name.
    // We use it to find attack animations (LightAttack, HeavyAttack, etc)
    internal static AnimationClip GetAnimationClipFromAnimatorByName(Animator animator, string name)
    {
        //can't get data if no animator
        if (animator == null)
            return null;

        //favor for above foreach due to performance issues
        for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
        {
            if (animator.runtimeAnimatorController.animationClips[i].name == name)
            {
                return animator.runtimeAnimatorController.animationClips[i];
            }
        }

        Debug.LogError("Animation clip: " + name + " not found");
        return null;
    }
}
