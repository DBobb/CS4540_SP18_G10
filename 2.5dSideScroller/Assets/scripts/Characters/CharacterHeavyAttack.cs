﻿// Author: Keaton Smith
// Time spent: 8 hours
// User Story: 31
// Class Purpose: This script allows the character objects to use feat attacks that require use of mana. It spawns a collider that has
// been placed on the character object. 
// Important to note that attackName is used for animation, animation trigger, and input name
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHeavyAttack : MonoBehaviour
{

    string attackName = "HeavyAttack";           // Light, Heavy, Feat Light, or Feat Heavy 
                                                 // ***IMPORTANT*** This name is case sensitive and must match the 
                                                 // input name, animation trigger name, and animation name

    public float damage;                // Amount of damage this attack deals to enemies

    public float manaCost;

    public float startActiveTime;       // A time when the hitbox object first become active during the animation
    public float endActiveTime;         // A time when the hitbox object becomes inactive during the animation

    public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn

    float startTime = 0f;               // Time tracker to note beginning of attack
    bool isAttacking = false;           // Trigger variable

    Animator animator;                  // PlayerAC
    AnimationClip groundAttackAnimation;      // Specific attack clip pulled from PlayerAC

    CharacterMovement characterMovement;// Used for disabling character movement during attack
    CharacterAttackManagement attackManagement;
    CharacterMana characterMana;
    CharacterJump characterJump;

    public GameObject specialScript;

    bool approvalFromManager = false;

    public ParticleSystem particleSystem;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        characterMovement = GetComponent<CharacterMovement>();
        characterMana = GetComponent<CharacterMana>();
        characterJump = GetComponent<CharacterJump>();
        particleSystem.Stop();
        attackBox.GetComponent<AttackHit>().damage = this.damage;

        attackBox.enabled = false; // Disable the hitbox upon spawn
        attackManagement = GetComponent<CharacterAttackManagement>();
        groundAttackAnimation = GetAnimationClipFromAnimatorByName(animator, attackName); // Pull out the attack animation clip from character
        startActiveTime = startActiveTime * groundAttackAnimation.length;
        endActiveTime = endActiveTime * groundAttackAnimation.length;
        if (specialScript != null)
        {
            if (CharacterManager.Instance.curChar.name.Contains("Itzuli"))
            {
                groundAttackAnimation = GetAnimationClipFromAnimatorByName(animator, attackName); // Pull out the attack animation clip from character
                specialScript.GetComponent<ItzuliHeavyAttack>().SetValues(groundAttackAnimation.length, damage);
            }
            else if (CharacterManager.Instance.curChar.name.Contains("Amelia")) specialScript.GetComponent<AmeliaHeavyAttack>().SetValues(damage);
            else if (CharacterManager.Instance.curChar.name.Contains("Florence")) groundAttackAnimation = GetAnimationClipFromAnimatorByName(animator, attackName); // Pull out the attack animation clip from character

            specialScript.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (approvalFromManager)
        {
            GroundHeavyAttack();
        }
        else if(isAttacking && !approvalFromManager)
        {
            isAttacking = false;

            animator.SetBool(attackName, false);
            attackBox.enabled = false;

            particleSystem.Stop();
            characterMovement.enabled = true;
            characterJump.SetCanJump(true);

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);

            if (specialScript != null) specialScript.SetActive(false);
            characterMovement.SetCanMove(true);
        }

    }

    void GroundHeavyAttack()
    {

        // Conditions Required to begin attacking
        if (!(CharacterManager.Instance.curChar.name.Contains("Amelia"))) // Amelia's attack properties are so different that we must do something completely different from the rest
        {
            FlorenceAndItzuliHeavy();
        }
        else if((CharacterManager.Instance.curChar.name.Contains("Amelia")))
        {
            AmeliaHeavy();
        }
    }

    void FlorenceAndItzuliHeavy()
    {
        if (!isAttacking && GetComponent<CharacterGrounded>().grounded)
        {
            if (UIManager.Instance.curCharManaSlider.value >= manaCost)
            {
                isAttacking = true;
                startTime = Time.time; // Get start time of when attack began
                animator.SetBool(attackName, true);

                //UIManager.Instance.curCharManaSlider.value -= manaCost;
                groundAttackAnimation = GetAnimationClipFromAnimatorByName(animator, attackName);
                CharacterManager.Instance.curChar.GetComponent<CharacterMana>().UseMana(manaCost);

            }
            else
            {
                isAttacking = false;

                animator.SetBool(attackName, false);
                attackBox.enabled = false;

                approvalFromManager = false;
                attackManagement.SetIsAttacking(false);

                if (specialScript != null) specialScript.SetActive(false);
            }
        }
        // Code to perform if character is attacking
        else if (isAttacking == true)
        {
            // Disable certain scripts
            characterJump.SetCanJump(false);
            characterMovement.enabled = false;
            particleSystem.Play();

            // Execute this while the attack animation lasts
            if (Time.time - startTime < groundAttackAnimation.length)
            {
                if (specialScript != null)
                {
                    specialScript.SetActive(true);
                }
                else
                {
                    // if we are at the active attacking times
                    if (Time.time - startTime >= startActiveTime && Time.time - startTime < endActiveTime)
                    {
                        attackBox.enabled = true;
                    }
                    // if we have exceeded the attacking times
                    else if (Time.time - startTime > endActiveTime)
                    {
                        attackBox.enabled = false;
                    }
                }
            }
            // Execute this after attack animation finishes
            else if (Time.time - startTime >= groundAttackAnimation.length)
            {
                isAttacking = false;

                animator.SetBool(attackName, false);
                attackBox.enabled = false;

                particleSystem.Stop();
                characterMovement.enabled = true;
                characterJump.SetCanJump(true);

                approvalFromManager = false;
                attackManagement.SetIsAttacking(false);

                if (specialScript != null) specialScript.SetActive(false);
            }
        }
    }
    void AmeliaHeavy()
    {
        if (specialScript.gameObject.activeSelf == false && Input.GetButton("HeavyAttack"))
        {
            animator.SetBool("HeavyAttack", true);
            specialScript.GetComponent<AmeliaHeavyAttack>().activated = true;
            particleSystem.Play();
            characterMovement.SetCanMove(false);
            characterJump.SetCanJump(false);
            specialScript.SetActive(true);
            isAttacking = true;
        }
        else if (isAttacking == false || specialScript.GetComponent<AmeliaHeavyAttack>().activated == false)
        {
            isAttacking = false;
            animator.SetBool(attackName, false);
            attackBox.enabled = false;

            specialScript.SetActive(false);

            particleSystem.Stop();
            characterMovement.SetCanMove(true);
            characterJump.SetCanJump(true);

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);

        }
    }

    
    // This is a function made to search for an animation clip in an animator by name.
    // We use it to find attack animations (LightAttack, HeavyAttack, etc)
    internal static AnimationClip GetAnimationClipFromAnimatorByName(Animator animator, string name)
    {
        //can't get data if no animator
        if (animator == null)
            return null;

        //favor for above foreach due to performance issues
        for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
        {
            if (animator.runtimeAnimatorController.animationClips[i].name == name)
            {
                return animator.runtimeAnimatorController.animationClips[i];
            }
        }

        Debug.LogError("Animation clip: " + name + " not found");
        return null;
    }

    public void SetManagerApproval(bool result)
    {
        approvalFromManager = result;
    }

    public void SetIsAttacking(bool result)
    {
        isAttacking = result;
    }
}
