﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttackManagement : MonoBehaviour {

    public bool isAttacking = false;
    bool lightGroundAttack = false;
    bool lightAirAttack = false;
    bool heavyGroundAttack = false;
    bool heavyAirAttack = false;
    public float lightAttackModifier;
    public float heavyAttackModifer;


    CharacterLightAttack characterLightAttack;
    CharacterLightAttackAir characterLightAttackAir;
    CharacterHeavyAttack characterHeavyAttack;
    CharacterHeavyAttackAir characterHeavyAttackAir;
    CharacterMovement characterMovement;
    CharacterJump characterJump;
    Animator animator;
    // Use this for initialization
    void Start () {
        characterLightAttack = GetComponent<CharacterLightAttack>();
        characterLightAttackAir = GetComponent<CharacterLightAttackAir>();
        characterHeavyAttack = GetComponent<CharacterHeavyAttack>();
        characterHeavyAttackAir = GetComponent<CharacterHeavyAttackAir>();

        animator = GetComponent<Animator>();

        characterMovement = GetComponent<CharacterMovement>();
        characterJump = GetComponent<CharacterJump>();
    }
	
	// Update is called once per frame
	void Update () {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Hit"))
        {
            SetIsAttacking(false);
        }
        if (isAttacking == false && animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") || animator.GetCurrentAnimatorStateInfo(0).IsName("Walk") || animator.GetCurrentAnimatorStateInfo(0).IsName("JumpDown") || animator.GetCurrentAnimatorStateInfo(0).IsName("JumpUp"))
        {
            if (Input.GetButton("LightAttack") && !isAttacking)
            {
                if (GetComponent<CharacterGrounded>().grounded)
                {
                    lightGroundAttack = true;
                }
                else if (!(GetComponent<CharacterGrounded>().grounded))
                {
                    lightAirAttack = true;
                }
                isAttacking = true;
            }
            if (Input.GetButtonDown("HeavyAttack") && !isAttacking)
            {
                if (GetComponent<CharacterGrounded>().grounded)
                {
                    heavyGroundAttack = true;
                }
                else if (!(GetComponent<CharacterGrounded>().grounded))
                {
                    heavyAirAttack = true;
                }
                isAttacking = true;
            }
        }

    }

    void FixedUpdate()
    {
        
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Hit"))
        {
            SetIsAttacking(false);
        }
        
        if (lightGroundAttack)
        {
            characterLightAttack.SetManagerApproval(true);
        }
        else if (lightAirAttack)
        {
            characterLightAttackAir.SetManagerApproval(true);
        }
        else if(heavyGroundAttack)
        {
            characterHeavyAttack.SetManagerApproval(true);
        }
        else if(heavyAirAttack)
        {
            characterHeavyAttackAir.SetManagerApproval(true);
        }
    }

    public void SetIsAttacking(bool result)
    {
        isAttacking = result;
        lightGroundAttack = false;
        lightAirAttack = false;
        heavyGroundAttack = false;
        heavyAirAttack = false;
        characterLightAttack.SetManagerApproval(false);
        characterLightAttackAir.SetManagerApproval(false);
        characterHeavyAttack.SetManagerApproval(false);
        characterHeavyAttackAir.SetManagerApproval(false);
        characterMovement.enabled = true;
        characterJump.SetCanJump(true);
    }
}
