﻿// Author: Keaton Smith
// Time spent: 8 hours
// User Story: 29
// Class Purpose: This script allows the character objects to use attacks. It spawns a collider that has
// been placed on the character object. 
// Important to note that attackName is used for animation, animation trigger, and input name
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLightAttack : MonoBehaviour
{

    string attackName = "LightAttack";           // Light or  Heavy 
                                        // ***IMPORTANT*** This name is case sensitive and must match the 
                                        // input name, animation trigger name, and animation name

    public float damage;                // Amount of damage this attack deals to enemies

    public float startActiveTime;       // A time when the hitbox object first become active during the animation
    public float endActiveTime;         // A time when the hitbox object becomes inactive during the animation
    public float animSpeed;

    public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn

    float startTime = 0f;               // Time tracker to note beginning of attack
    bool isAttacking = false;           // Trigger variable

    Animator animator;                  // PlayerAC
    AnimationClip groundAttackAnimation;      // Specific attack clip pulled from PlayerAC

    CharacterMovement characterMovement;// Used for disabling character movement during attack
    CharacterJump characterJump;
    CharacterAttackManagement attackManagement;
    public ParticleSystem particleSystem;

    bool approvalFromManager = false; // this is tied tothe enemy attack manager

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        characterMovement = GetComponent<CharacterMovement>();
        characterJump = GetComponent<CharacterJump>();
        attackManagement = GetComponent<CharacterAttackManagement>();
        groundAttackAnimation = GetAnimationClipFromAnimatorByName(animator, attackName); // Pull out the attack animation clip from character

        attackBox.enabled = false; // Disable the hitbox upon spawn
        attackBox.GetComponent<AttackHit>().damage = this.damage;
        
        startActiveTime = startActiveTime * groundAttackAnimation.length;
        endActiveTime = endActiveTime * groundAttackAnimation.length; 
    }

    void FixedUpdate()
    {
        if (approvalFromManager)
        {
            GroundAttack();
        }
        else if (isAttacking && !approvalFromManager)
        {
            isAttacking = false;

            animator.SetBool(attackName, false);
            attackBox.enabled = false;

            particleSystem.Stop();
            characterMovement.enabled = true;
            characterJump.SetCanJump(true);

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);
            characterMovement.SetCanMove(true);
        }
    }

    // Update is called once per frame
    void GroundAttack()
    {
        // Conditions Required to begin attacking
        if ( !isAttacking && GetComponent<CharacterGrounded>().grounded)
        {
            startTime = Time.time; // Get start time of when attack began
            animator.SetBool(attackName, true);
            isAttacking = true;
        }
        // Code to perform if character is attacking
        else if (isAttacking == true)
        {
            // Disable certain scripts
            characterMovement.enabled = false;
            characterJump.SetCanJump(false); 
            // Execute this while the attack animation lasts
            if (Time.time - startTime < groundAttackAnimation.length)
            {
                particleSystem.Play();

                // if we are at the active attacking times
                if (Time.time - startTime >= startActiveTime && Time.time - startTime < endActiveTime) 
                {
                    
                    attackBox.enabled = true;
                }
                // if we have exceeded the attacking times
                else if (Time.time - startTime > endActiveTime) 
                {
                    attackBox.enabled = false;
                }
            }
            // Execute this after attack animation finishes
            else if (Time.time - startTime >= groundAttackAnimation.length)
            {
                isAttacking = false;
                
                animator.SetBool(attackName, false);
                attackBox.enabled = false;

                particleSystem.Stop();
                characterMovement.enabled = true;
                characterJump.SetCanJump(true);
                approvalFromManager = false;
                attackManagement.SetIsAttacking(false);
            }
        }
        
    }

    // This is a function made to search for an animation clip in an animator by name.
    // We use it to find attack animations (LightAttack, HeavyAttack, etc)
    internal static AnimationClip GetAnimationClipFromAnimatorByName(Animator animator, string name)
    {
        //can't get data if no animator
        if (animator == null)
            return null;

        //favor for above foreach due to performance issues
        for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
        {
            if (animator.runtimeAnimatorController.animationClips[i].name == name)
            {
              
                return animator.runtimeAnimatorController.animationClips[i];
            }
        }

        Debug.LogError("Animation clip: " + name + " not found");
        return null;
    }

    public void SetManagerApproval(bool result)
    {
        approvalFromManager = result;
    }

}