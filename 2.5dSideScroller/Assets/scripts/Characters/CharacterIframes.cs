﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterIframes : MonoBehaviour {


	public float timeActive;
	public float sTime;

	// Use this for initialization
	void Start () {
		InvincibilityManager.Instance.OnHit += Invincible;
		//InvincibilityManager.Instance.OnHit += Flash;

	}

    // Update is called once per frame
    void Update()
    {
        if (InvincibilityManager.Instance.intangible)
        {
            CharacterManager.Instance.curChar.layer = 15;
        }
        else
        {
            CharacterManager.Instance.curChar.layer = 13;
        }
    }

	void Invincible() {



		float sTime = CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().sTime;
		float timeActive = CharacterManager.Instance.curChar.GetComponent<CharacterIframes> ().timeActive;
		//Might have issue here with resetting start time
		//float startTime = Time.time;
		float wTime = Time.time - sTime;
		if (wTime < timeActive) {
			CharacterManager.Instance.curChar.gameObject.GetComponent<CharacterHealth> ().invincible = true;
			InvincibilityManager.Instance.intangible = true;




		} else {

			InvincibilityManager.Instance.iTrigger = false;
			InvincibilityManager.Instance.intangible = false;
			CharacterManager.Instance.curChar.gameObject.GetComponent<CharacterHealth> ().invincible = false;
			CharacterManager.Instance.curChar.layer = 13;

		}

	}

	void OnCollisionEnter(Collision collision)
	{
		if (InvincibilityManager.Instance.intangible) {

			//Change layer to intangible zone
			CharacterManager.Instance.curChar.layer = 15;
			/*
			if (collision.gameObject.tag == "Enemy")
			{
				Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
			}*/

		}
	}

	void Flash()
	{

		GameObject curChar = CharacterManager.Instance.curChar.gameObject;
		float startTime = 0;
		bool getStart = true;

		if (getStart) {
			startTime = Time.time;
			getStart = false;
		}
			
		
		float duration = 1.0f;

		if (Time.time - startTime < duration) {
			//curChar.GetComponent<Renderer> ().enabled = curChar.GetComponent<Renderer> ().enabled;
		} else {
			getStart = true;
			//curChar.GetComponent<Renderer> ().enabled = !curChar.GetComponent<Renderer> ().enabled;
		}
		/*
		int renderSwitch = Mathf.RoundToInt (Time.time);
		if (InvincibilityManager.Instance.intangible) {
			if (renderSwitch % 2 == 0)
				curChar.GetComponent<Renderer> ().enabled = false;
			else
				curChar.GetComponent<Renderer> ().enabled = true;
		
		} else {
			curChar.GetComponent<Renderer> ().enabled = true;
		}
	}*/

	}
}
