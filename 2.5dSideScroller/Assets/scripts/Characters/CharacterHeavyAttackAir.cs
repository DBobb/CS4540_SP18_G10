﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHeavyAttackAir : MonoBehaviour
{

    string attackName = "HeavyAttackAir";           // Light or  Heavy 
                                                        // ***IMPORTANT*** This name is case sensitive and must match the 
                                                        // input name, animation trigger name, and animation name

    public float damage;                // Amount of damage this attack deals to enemies

    public float startActiveTime;       // A time when the hitbox object first become active during the animation
    public float endActiveTime;         // A time when the hitbox object becomes inactive during the animation

    //public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn

    float startTime = 0f;               // Time tracker to note beginning of attack
    bool isAttacking = false;           // Trigger variable

    public float manaCost;

    Animator animator;                  // PlayerAC
    AnimationClip airAttackAnimation;      // Specific attack clip pulled from PlayerAC

    CharacterMovement characterMovement;// Used for disabling character movement during attack
    CharacterJump characterJump;        // Used for disabling character jumping during attack
    CharacterAttackManagement attackManagement;

    public ParticleSystem particleSystem;
    public GameObject specialScript;
    public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn

    bool approvalFromManager = false; // this is tied tothe enemy attack manager

    // Current character variables
    GameObject curChar;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        characterMovement = GetComponent<CharacterMovement>();
        characterJump = GetComponent<CharacterJump>();
        attackManagement = GetComponent<CharacterAttackManagement>();

        airAttackAnimation = GetAnimationClipFromAnimatorByName(animator, attackName);
        attackBox.GetComponent<AttackHit>().damage = this.damage;
        startActiveTime = startActiveTime * airAttackAnimation.length;
        endActiveTime = endActiveTime * airAttackAnimation.length;

        // attackBox.enabled = false; // Disable the hitbox upon spawn
        // attackBox.GetComponent<AttackHit>().damage = this.damage;
        if (specialScript != null)
        {
            if(CharacterManager.Instance.curChar.name.Contains("Itzuli")) specialScript.GetComponent<ItzuliHeavyAttackAir>().SetValues(airAttackAnimation.length, damage);
            else if (CharacterManager.Instance.curChar.name.Contains("Amelia")) specialScript.GetComponent<AmeliaHeavyAttackAir>().SetValues(damage);

            specialScript.SetActive(false);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (approvalFromManager)
        {
            AirAttack();
        }
        else if (isAttacking && !approvalFromManager)
        {
            if (specialScript != null)
            {
                specialScript.SetActive(true);
            }
            animator.SetBool(attackName, false);
            attackBox.enabled = false;

            particleSystem.Stop();

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);
            isAttacking = false;
        }
    }
    void AirAttack()
    {
        // Conditions Required to begin attacking
        if (!isAttacking)// && pm.curCharManaSlider.value >= manaCost)
        {

            if (UIManager.Instance.curCharManaSlider.value >= manaCost)
            {
                isAttacking = true;


                startTime = Time.time; // Get start time of when attack began
                animator.SetBool(attackName, true);
                //UIManager.Instance.curCharManaSlider.value -= manaCost;
                CharacterManager.Instance.curChar.GetComponent<CharacterMana>().UseMana(manaCost);

            }
            else
            {
                isAttacking = false;

                animator.SetBool(attackName, false);
                attackBox.enabled = false;

                approvalFromManager = false;
                attackManagement.SetIsAttacking(false);

            }
        }
        // Code to perform if character is attacking
        else if (isAttacking == true)
        {

            if (CharacterManager.Instance.curChar.name.Contains("Itzuli")) // If Itzuli, attack triggers on landing
            {
                ItzuliHeavy();
            }
            else if (CharacterManager.Instance.curChar.name.Contains("Amelia")) // if you are amelia, you attack when you press the Button
            {
                AmeliaHeavy();

            }
            else if (CharacterManager.Instance.curChar.name.Contains("Florence")) // if you are Florence, you attack the whole way down
            {
                FlorenceHeavy();
            }
        }
       
    }

    void ItzuliHeavy()
    {
        particleSystem.Play();

        // Execute this after attack animation finishes
        if (GetComponent<CharacterGrounded>().grounded) //(Time.time - startTime >= groundAttackAnimation.length)
        {
            // If there is a special script, let that change the animation, else end animation on hitting the ground
            if (specialScript != null)
            {
                specialScript.SetActive(true);
            }


            animator.SetBool(attackName, false);

            // attackBox.enabled = false;

            particleSystem.Stop();

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);
            isAttacking = false;


        }
    }

    void AmeliaHeavy()
    {
        if (specialScript.GetComponent<AmeliaHeavyAttackAir>().activated == false)
        {
            specialScript.GetComponent<AmeliaHeavyAttackAir>().activated = true;
            specialScript.GetComponent<AmeliaHeavyAttackAir>().SetValues(damage);
            specialScript.SetActive(true);
            particleSystem.Play();
        }
        else if (GetComponent<CharacterGrounded>().grounded || Time.time - startTime > airAttackAnimation.length)//specialScript.GetComponent<AmeliaHeavyAttack>().activated == false)
        {
            animator.SetBool(attackName, false);
            particleSystem.Stop();
            animator.SetBool("HeavyFire", false);

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);
            isAttacking = false;
            specialScript.GetComponent<AmeliaHeavyAttackAir>().SetValues(damage);
            specialScript.GetComponent<AmeliaHeavyAttackAir>().activated = false;
            specialScript.GetComponent<AmeliaHeavyAttackAir>().ameliaHandArrow.SetActive(false);

        }
    }

    void FlorenceHeavy()
    {
        characterMovement.enabled = false;
        characterJump.SetCanJump(false);

        particleSystem.Play();

        // Execute this while the attack animation lasts
        if (Time.time - startTime < airAttackAnimation.length)
        {

            // if we are at the active attacking times
            if (Time.time - startTime >= startActiveTime && Time.time - startTime < endActiveTime)
            {
                attackBox.enabled = true;
            }
            // if we have exceeded the attacking times
            else if (Time.time - startTime > endActiveTime)
            {
                attackBox.enabled = false;
            }

        }
        // Execute this after attack animation finishes
        else if (Time.time - startTime >= airAttackAnimation.length && GetComponent<CharacterGrounded>().grounded)
        {
            isAttacking = false;

            animator.SetBool(attackName, false);
            attackBox.enabled = false;

            particleSystem.Stop();
            characterMovement.enabled = true;
            characterJump.SetCanJump(true);

            approvalFromManager = false;
            attackManagement.SetIsAttacking(false);

        }
    
    }

    // This is a function made to search for an animation clip in an animator by name.
    // We use it to find attack animations (LightAttack, HeavyAttack, etc)
    internal static AnimationClip GetAnimationClipFromAnimatorByName(Animator animator, string name)
    {
        //can't get data if no animator
        if (animator == null)
            return null;

        //favor for above foreach due to performance issues
        for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i++)
        {
            if (animator.runtimeAnimatorController.animationClips[i].name == name)
            {
                return animator.runtimeAnimatorController.animationClips[i];
            }
        }

        Debug.LogError("Animation clip: " + name + " not found");
        return null;
    }

    public void SetManagerApproval(bool result)
    {
        approvalFromManager = result;
    }

    public void SetIsAttacking(bool result)
    {
        isAttacking = result;
    }
}
