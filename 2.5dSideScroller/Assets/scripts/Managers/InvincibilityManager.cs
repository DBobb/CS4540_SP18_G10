﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityManager : SingletonPersistant<InvincibilityManager> {

	public bool iTrigger = false;
	public bool intangible = false;

	// Update is called once per frame
	void Update () {

		if (iTrigger)
			OnHit ();
	}
	//Declaring my delegate
	public delegate void GotHitDelegate();
	//Creating an instance of that delegate for scripts to add to.
	public GotHitDelegate OnHit;
}
