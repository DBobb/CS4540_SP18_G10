﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

// Author: Robert Bobbitt
// Time spent: 5 hours
// User Story: N/A
// Class Purpose:
//This manager is to behave as a Persistant Singleton. Meaning there should only be one of it's kind, and it'll be handling Characters.

public class CharacterManager : Singleton<CharacterManager> {


	// ****** Character Switching Variables ******
	public GameObject char1;
	public GameObject char2;
	public GameObject char3;
	public GameObject curChar;
    List <GameObject> charAry;
    //public GameObject prevChar1;
    //public GameObject prevChar2;
    public float coolDownTime = 5f;
    List <float> startTimes;
    float[] strTimes;
	public bool swap = false;
    public int curCharNum = 0;
	// Use this for initialization
	void Awake()
	{
        charAry = new List<GameObject>();
        startTimes = new List<float>();
        //Calling original awake function

        // Create char1, Set char1 as the active player
        char1 = (GameObject)Instantiate(char1, transform.position, transform.rotation);
		char1.SetActive(true);
		curChar = char1;

		//Create char2, set char2 as the inactive player
		char2 = (GameObject)Instantiate(char2, transform.position, transform.rotation);
		char2.SetActive(false);

		//Create char3, set char3 as the inactive player
		char3 = (GameObject)Instantiate(char3, transform.position, transform.rotation);
		char3.SetActive(false);

        charAry.Add(char1);
        charAry.Add(char2);
        charAry.Add(char3);

        startTimes.Add(0);
        startTimes.Add(0);
        startTimes.Add(0);

    }

    // Update is called once per frame
    void Start()
    {
        int index = GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().visitedLodeStone.FindLastIndex(x => x == true);

        if (GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneScene[index] == SceneManager.GetActiveScene().name)
        {
            CharacterManager.Instance.curChar.transform.position = new Vector3(GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneList[index], 0, 0);
        }
        else
        {
            CharacterManager.Instance.curChar.transform.position = new Vector3(-17, 0, 0);
        }

        if (SaveManager.Instance.warpingPressed == true)
        {
            SaveManager.Instance.warpingPressed = false;
            CharacterManager.Instance.curChar.transform.position = new Vector3(SaveManager.Instance.warpXTo, 0, 0);
        }        

        gameObject.transform.position = curChar.transform.position;
 
    }
    void Update()
	{
        
		gameObject.transform.position = curChar.transform.position;
		if(curChar.GetComponent<CharacterGrounded>().grounded && curChar.GetComponent<CharacterAttackManagement>().isAttacking == false) SwitchChars();

        if (startTimes[0] > 0) startTimes[0] -= Time.deltaTime;
        if (startTimes[1] > 0) startTimes[1] -= Time.deltaTime;
        if (startTimes[2] > 0) startTimes[2] -= Time.deltaTime;

        coolDownOverlay();
    }

    void SwitchChars()
	{
		// If the player presses the '1' button, switch to char1
		if (Input.GetButtonDown ("SwitchChar1"))// && prevChar1 && prevChar1.GetComponent<CharacterHealth>().isDead == false) {
        {
            if (charAry[(curCharNum + 2) % 3].GetComponent<CharacterHealth>().isDead == false && startTimes[(curCharNum + 2) % 3] <= 0)
            {
                SwitchUp();
            }
            else if ((charAry[(curCharNum + 1) % 3].GetComponent<CharacterHealth>().isDead == false) && startTimes[(curCharNum + 1) % 3] <= 0)
            {
                SwitchDown();
            }
        }
		// If the player presses the '2' button, switch to char2
		else if (Input.GetButtonDown ("SwitchChar2")) 
        {
            if (charAry[(curCharNum + 1) % 3].GetComponent<CharacterHealth>().isDead == false && startTimes[(curCharNum + 1) % 3] <= 0)
            {
                SwitchDown();
            }
            else if((charAry[(curCharNum + 2) % 3].GetComponent<CharacterHealth>().isDead == false) && startTimes[(curCharNum + 2) % 3] <= 0)
            {
                SwitchUp();
            }
        }
        else if (curChar.GetComponent<CharacterHealth>().isDead == true )// && prevChar2)
        {
            GameObject hold;
            swap = true;
            curChar.SetActive(false);

            char1.transform.position = curChar.transform.position;
            char1.transform.rotation = curChar.transform.rotation;

            hold = curChar;
            curChar = char1;
            //prevChar2 = hold;

            curChar.SetActive(true);
            InvincibilityManager.Instance.iTrigger = false;
            InvincibilityManager.Instance.intangible = false;
        }

        else {
			swap = false;
		}

	}

    public void SwitchUp()
    {
        
        startTimes[curCharNum] = coolDownTime;
        curCharNum = (curCharNum + 2) % 3;

        swap = true;


        // Disable currently active player
        curChar.SetActive(false);
        charAry[curCharNum].transform.position = curChar.transform.position;

        //Set Active Player, Store Previous Character
        curChar = charAry[curCharNum];

        curChar.SetActive(true);

        InvincibilityManager.Instance.iTrigger = false;
        InvincibilityManager.Instance.intangible = false;
    }

    public void SwitchDown()
    {
        startTimes[curCharNum] = coolDownTime;
        curCharNum = (curCharNum + 1) % 3;

        swap = true;

        // Disable currently active player
        curChar.SetActive(false);
        charAry[curCharNum].transform.position = curChar.transform.position;
        

        //Set Active Player, Store Previous Character
        curChar = charAry[curCharNum];

        curChar.SetActive(true);
       
        InvincibilityManager.Instance.iTrigger = false;
        InvincibilityManager.Instance.intangible = false;
    }


    void coolDownOverlay()
    {
        if (!char1.GetComponent<CharacterHealth>().isDead)
        {
            UIManager.Instance.Char1HealthCoolDownSlider.value = startTimes[0];
            UIManager.Instance.Char1ManaCoolDownSlider.value = startTimes[0];
        }
        if (!char2.GetComponent<CharacterHealth>().isDead)
        {
            UIManager.Instance.Char2HealthCoolDownSlider.value = startTimes[1];
            UIManager.Instance.Char2ManaCoolDownSlider.value = startTimes[1];
        }
        if (!char3.GetComponent<CharacterHealth>().isDead)
        {
            UIManager.Instance.Char3HealthCoolDownSlider.value = startTimes[2];
            UIManager.Instance.Char3ManaCoolDownSlider.value = startTimes[2];
        }
    }

    void StopParticleSystem()
    {
        curChar.transform.GetChild(0).gameObject.SetActive(false);
    }

}
