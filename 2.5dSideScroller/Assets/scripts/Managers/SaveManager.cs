﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

// Author: Robert Bobbitt
// Time spent: 5 hours
// User Story: 22
// Class Purpose: To manage player save data.
//This manager is to behave as a Singleton. Meaning there should only be one of it's kind, and it'll be handling save data.
public class SaveManager : SingletonPersistant<SaveManager> {

    
	//Declaring our Save Manager Object
	public static SaveManager saveManager;

    //Declaring the data that is to be saved which is currently only position. 
    //Note: All save data must be declared below separately.

    public int softBisCount;

    public bool saved = false; // Keaton added this variable to help with the Enemy Spawners so that they can respawn after player has saved;
    public bool continuePressed = false;
    public bool warpingPressed = false;
    public bool death = false;
    GameObject popUpPanel;
    GameObject inventory;
    public float warpXTo;
    string stringCount;
    public int levelOfEnemy;
    public List<string> fakeMISC;
    public List<string> fakeMISN;
    public bool saveInGame = false;
    public float AHPMax, AMPMax, IHPMax, IMPMax, FHPMax, FMPMax;
    public int ACost, ICost, FCost;
    public float AH, AL, IH, IL, FH, FL;
    public bool onMenu = false;
    public bool continueGame = false;

    //This function is what is called to save the game, which essentially writes and encodes
    //game data to a binary file to be loaded for later. The file it writes to can be found in "/AppData"
    void InitializeLists()
    {
        
        fakeMISC = new List<string>();
        fakeMISN = new List<string>();
        if (saveInGame == true)
        {
            for (int i = 0; i < GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).childCount - 1; i++)
            {
                fakeMISC.Insert(i, "");
                fakeMISN.Insert(i, "");
            }
            
            for (int i = 0; i < GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).childCount - 1; i++)
            {
                fakeMISN[i] = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).transform.GetChild(i).transform.GetChild(0).GetComponent<Text>().text;
                fakeMISC[i] = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).transform.GetChild(i).transform.GetChild(1).GetComponent<Text>().text;
            }
        }
        
    }
    public void Save()
	{
		//Creating a BinaryFormatter and File
 		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");

        //Creating an Object to save the data to
        PlayerData data = new PlayerData();
        if (onMenu)
        {
            data.audioVolume = AudioListener.volume;
        }
        else
        {
            InitializeLists();
            saved = true;
            continueGame = true;
            data.audioVolume = AudioListener.volume;
            data.xPosition = GameObject.Find("WarpingCanvas").GetComponent<PerformWarp>().xPositionWarp;

            data.lsBoolList = GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().visitedLodeStone;
            data.lsPositionList = GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneList;
            data.lsSceneList = GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneScene;

            data.s0Status = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus[0];
            data.s1Status = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus[1];
            data.s2Status = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus[2];
            data.s3Status = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus[3];

            data.s0Count = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts[0].ToString();
            data.s1Count = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts[1].ToString();
            data.s2Count = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts[2].ToString();
            data.s3Count = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts[3].ToString();

            data.MISlotStatus = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).GetComponent<MainInventory>().slotStatus;

            data.MISlotName = fakeMISN;
            data.MISlotCount = fakeMISC;

            data.MIItems = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).GetComponent<AddToMainInv>().itemsList;

            data.sbCount = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount;

            data.enemyLevel = GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().visitedLodeStone.FindAll(x => x == true).Count;

            data.ACharl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel;

            data.Ahplvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().hpLevel;
            data.Amplvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().mpLevel;
            data.Aspeedlvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().speedLevel;
            data.Alightlvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().lightLevel;
            data.Aheavylvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().heavyLevel;
            data.AhsMax = UIManager.Instance.char1HealthSlider.maxValue;
            data.AmsMax = UIManager.Instance.Char1ManaSlider.maxValue;
            data.ArunSpeed = CharacterManager.Instance.char1.GetComponent<CharacterMovement>().runSpeed;
            data.AlightModifier = CharacterManager.Instance.char1.GetComponent<CharacterAttackManagement>().lightAttackModifier;
            data.AheavyModifier = CharacterManager.Instance.char1.GetComponent<CharacterAttackManagement>().heavyAttackModifer;
            data.AroundedCost = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(12).gameObject.GetComponent<CalculateCost>().roundedCost;

            data.ICharl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel;
            data.Ihplvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().hpLevel;
            data.Implvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().mpLevel;
            data.Ispeedlvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().speedLevel;
            data.Ilightlvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().lightLevel;
            data.Iheavylvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().heavyLevel;
            data.IhsMax = UIManager.Instance.char2HealthSlider.maxValue;
            data.ImsMax = UIManager.Instance.Char2ManaSlider.maxValue;
            data.IrunSpeed = CharacterManager.Instance.char2.GetComponent<CharacterMovement>().runSpeed;
            data.IlightModifier = CharacterManager.Instance.char2.GetComponent<CharacterAttackManagement>().lightAttackModifier;
            data.IheavyModifier = CharacterManager.Instance.char2.GetComponent<CharacterAttackManagement>().heavyAttackModifer;
            data.IroundedCost = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).transform.GetChild(12).gameObject.GetComponent<CalculateCost>().roundedCost;

            data.FCharl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel;
            data.Fhplvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().hpLevel;
            data.Fmplvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().mpLevel;
            data.Fspeedlvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().speedLevel;
            data.Flightlvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().lightLevel;
            data.Fheavylvl = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().heavyLevel;
            data.FhsMax = UIManager.Instance.char3HealthSlider.maxValue;
            data.FmsMax = UIManager.Instance.Char3ManaSlider.maxValue;
            data.FrunSpeed = CharacterManager.Instance.char3.GetComponent<CharacterMovement>().runSpeed;
            data.FlightModifier = CharacterManager.Instance.char3.GetComponent<CharacterAttackManagement>().lightAttackModifier;
            data.FheavyModifier = CharacterManager.Instance.char3.GetComponent<CharacterAttackManagement>().heavyAttackModifer;
            data.FroundedCost = GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).transform.GetChild(12).gameObject.GetComponent<CalculateCost>().roundedCost;
        }

        //Writing the data object to a file and closing it.
        bf.Serialize(file, data);
        file.Close();
    }

	//This function is what is called to load the game, which accesses the binary file returns the 
	//stored information.
	public bool Load()
	{
		//Check to see if the file exists before attempting to retrieve it.
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")) {
			//Load and open the file
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

            //Decode the data
            PlayerData data = (PlayerData)bf.Deserialize (file);
			file.Close ();

            if(onMenu)
            {
                AudioListener.volume = data.audioVolume;
            }
            else
            {
                AudioListener.volume = data.audioVolume;

                CharacterManager.Instance.curChar = CharacterManager.Instance.char1;

                GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneList = data.lsPositionList;
                GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().visitedLodeStone = data.lsBoolList;
                GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneScene = data.lsSceneList;

                if (continuePressed)
                {
                    int index = GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().visitedLodeStone.FindLastIndex(x => x == true);
                    SceneManager.LoadScene(GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneScene[index]);
                    CharacterManager.Instance.curChar.transform.position = new Vector3(GameObject.Find("Lodestones").GetComponent<LodeStonePoints>().lodeStoneList[index], 0, 0);
                    continuePressed = false;
                }

                if (warpingPressed)
                {
                    warpXTo = data.xPosition;
                    CharacterManager.Instance.transform.position = new Vector3(data.xPosition, 0, 0);
                }

                levelOfEnemy = data.enemyLevel;

                CharacterManager.Instance.char1.GetComponent<CharacterHealth>().isDead = false;
                CharacterManager.Instance.char2.GetComponent<CharacterHealth>().isDead = false;
                CharacterManager.Instance.char3.GetComponent<CharacterHealth>().isDead = false;

                CharacterManager.Instance.char1.GetComponent<CharacterHealth>().currentHealth = CharacterManager.Instance.char1.GetComponent<CharacterHealth>().startingHealth;
                CharacterManager.Instance.char1.GetComponent<CharacterMana>().curMana = CharacterManager.Instance.char1.GetComponent<CharacterMana>().startingMana;
                CharacterManager.Instance.char2.GetComponent<CharacterHealth>().currentHealth = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth;
                CharacterManager.Instance.char2.GetComponent<CharacterMana>().curMana = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana;
                CharacterManager.Instance.char3.GetComponent<CharacterHealth>().currentHealth = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth;
                CharacterManager.Instance.char3.GetComponent<CharacterMana>().curMana = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana;

                UIManager.Instance.char1HealthSlider.value = CharacterManager.Instance.char1.GetComponent<CharacterHealth>().startingHealth;
                UIManager.Instance.Char1ManaSlider.value = CharacterManager.Instance.char1.GetComponent<CharacterMana>().startingMana;
                UIManager.Instance.char2HealthSlider.value = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth;
                UIManager.Instance.Char2ManaSlider.value = CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana;
                UIManager.Instance.char3HealthSlider.value = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth;
                UIManager.Instance.Char3ManaSlider.value = CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana;

                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus.Insert(0, data.s0Status);
                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus.Insert(1, data.s1Status);
                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus.Insert(2, data.s2Status);
                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().slotStatus.Insert(3, data.s3Status);

                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts.Insert(0, data.s0Count);
                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts.Insert(1, data.s1Count);
                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts.Insert(2, data.s2Count);
                GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().allSlotCounts.Insert(3, data.s3Count);

                if (death == true)
                {
                    GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount = 0;
                    death = false;
                }
                else
                {
                    GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount = data.sbCount;
                }

                GameObject.Find("SoftBismuthCount").GetComponent<Text>().text = GameObject.Find("PlayerManager").GetComponent<SoftBismuthDrop>().playerSoftBismuthAmount.ToString();

                fakeMISC = data.MISlotCount;
                fakeMISN = data.MISlotName;

                for (int i = 0; i < 9; i++)
                {
                    GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).transform.GetChild(i).transform.GetChild(0).GetComponent<Text>().text = data.MISlotName[i];
                    GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).transform.GetChild(i).transform.GetChild(1).GetComponent<Text>().text = data.MISlotCount[i];
                    GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(3).transform.GetChild(2).transform.GetChild(i).transform.GetChild(0).GetComponent<Text>().text = data.MISlotName[i];
                    GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(3).transform.GetChild(2).transform.GetChild(i).transform.GetChild(1).GetComponent<Text>().text = data.MISlotCount[i];
                }

                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).GetComponent<MainInventory>().slotStatus = data.MISlotStatus;

                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(1).GetComponent<AddToMainInv>().itemsList = data.MIItems;


                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel = data.ACharl;
                Debug.Log(data.ACharl);
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().hpLevel = data.Ahplvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().mpLevel = data.Amplvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().speedLevel = data.Aspeedlvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().lightLevel = data.Alightlvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).gameObject.GetComponent<UpgradeAmelia>().heavyLevel = data.Aheavylvl;
                AHPMax = data.AhsMax;
                AMPMax = data.AmsMax;
                CharacterManager.Instance.char1.GetComponent<CharacterMovement>().runSpeed = data.ArunSpeed;
                AL = data.AlightModifier;
                AH = data.AheavyModifier;
                ACost = data.AroundedCost;

                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel = data.ICharl; ;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().hpLevel = data.Ihplvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().mpLevel = data.Implvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().speedLevel = data.Ispeedlvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().lightLevel = data.Ilightlvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<UpgradeItzuli>().heavyLevel = data.Ihplvl;
                IHPMax = data.IhsMax;
                IMPMax = data.ImsMax;
                CharacterManager.Instance.char2.GetComponent<CharacterMovement>().runSpeed = data.IrunSpeed;
                IL = data.IlightModifier;
                IH = data.IheavyModifier;
                ICost = data.IroundedCost;

                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).transform.GetChild(1).gameObject.GetComponent<UpdateCharLevel>().charLevel = data.FCharl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().hpLevel = data.Fhplvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().mpLevel = data.Fmplvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().speedLevel = data.Fspeedlvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().lightLevel = data.Flightlvl;
                GameObject.Find("LSMenuCanvas").transform.GetChild(0).transform.GetChild(0).transform.GetChild(5).gameObject.GetComponent<UpgradeFlorence>().heavyLevel = data.Fheavylvl;
                FHPMax = data.FhsMax;
                FMPMax = data.FmsMax;
                CharacterManager.Instance.char3.GetComponent<CharacterMovement>().runSpeed = data.FrunSpeed;
                FL = data.FlightModifier;
                FH = data.FheavyModifier;
                FCost = data.FroundedCost;

                InventoryUIUpdate(); // refreshes the UI with the loaded 
            }
            return true;
		}

		return false;
	}

	//This function is called to delete the binary file that holds the save data.
	public void Delete()
	{
		//Check to make sure the file we want to delete exists
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {
			//Delete said file
			File.Delete (Application.persistentDataPath + "/playerInfo.dat");
            Debug.Log("DELETING");
		}
	}

    [Serializable] //Allows us to write data to a file
                   //Note: the class PlayerData should hold all data that is intended to be saved between games.
    public class PlayerData
    {
        public float xPosition;
        public float yPosition;
        public float zPosition;
        public string scene;
        public List<bool> lsBoolList;
        public List<float> lsPositionList;
        public List<string> lsSceneList;

        public bool s0Status;
        public bool s1Status;
        public bool s2Status;
        public bool s3Status;

        public string s0Count;
        public string s1Count;
        public string s2Count;
        public string s3Count;

        public int sbCount;

        public List<bool> MISlotStatus;
        public List<string> MIItems;
        public List<string> MISlotName;
        public List<string> MISlotCount;
        public int enemyLevel;

        public int ACharl;
        public int Ahplvl;
        public int Amplvl;
        public int Aspeedlvl;
        public int Alightlvl;
        public int Aheavylvl;
        public float AhsMax;
        public float AmsMax;
        public float ArunSpeed;
        public float AlightModifier;
        public float AheavyModifier;
        public int AroundedCost;


        public int ICharl;
        public int Ihplvl;
        public int Implvl;
        public int Ispeedlvl;
        public int Ilightlvl;
        public int Iheavylvl;
        public float IhsMax;
        public float ImsMax;
        public float IrunSpeed;
        public float IlightModifier;
        public float IheavyModifier;
        public int IroundedCost;

        public int FCharl;
        public int Fhplvl;
        public int Fmplvl;
        public int Fspeedlvl;
        public int Flightlvl;
        public int Fheavylvl;
        public float FhsMax;
        public float FmsMax;
        public float FrunSpeed;
        public float FlightModifier;
        public float FheavyModifier;
        public int FroundedCost;

        public float audioVolume;
    }


    void InventoryUIUpdate()
    {
        AddItem add = GameObject.Find("PlayerManager").GetComponent<AddItem>();
        Inventory inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
        GameObject slotObject;
        RectTransform slotRect;

        int count = GameObject.FindGameObjectsWithTag("CalciteIcon").Length;
        int count2 = GameObject.FindGameObjectsWithTag("CupriteIcon").Length;

        for (int i = 0; i < count; i++){
            Destroy(GameObject.FindGameObjectsWithTag("CalciteIcon")[i]);
        }
        for (int i = 0; i < count2; i++){
            Destroy(GameObject.FindGameObjectsWithTag("CupriteIcon")[i]);
            Destroy(GameObject.FindGameObjectsWithTag("CupriteIcon")[i]);
        }

        for (int i = 0; i < 4; i++){
            inventory.gameObjectCounts[i].text = inventory.allSlotCounts[i];
        }

        for (int i = 0; i < 4; i++)
        {
            if (inventory.slotStatus[i] == false)
            {

                if (add.itemsList[i] == "Calcite")
                {
                    slotObject = (GameObject)Instantiate(add.calciteSlotImage);
                    slotRect = slotObject.GetComponent<RectTransform>();
                    slotRect.transform.SetParent(inventory.allSlots[i].transform);
                    slotRect.localPosition = new Vector3(30, -54, 0);
                    slotRect.localScale = new Vector3(1, 1, 1);
                }
                else if (add.itemsList[i] == "Cuprite")
                {
                    slotObject = (GameObject)Instantiate(add.cupriteSlotImage);
                    slotRect = slotObject.GetComponent<RectTransform>();
                    slotRect.transform.SetParent(inventory.allSlots[i].transform);
                    slotRect.localPosition = new Vector3(30, -54, 0);
                    slotRect.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }
}

