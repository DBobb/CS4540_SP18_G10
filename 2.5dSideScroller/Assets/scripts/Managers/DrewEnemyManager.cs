﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Author: Robert Bobbitt
// Time spent: 1 hour
// User Story: 41
// Class Purpose: Instantiate delegate that allows enemies to be powered up.
public class DrewEnemyManager : SingletonPersistant<DrewEnemyManager> {


	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.P))
			OnPowerUp ();
	}
	//Declaring my delegate
	public delegate void PowerUpDelegate();
	//Creating an instance of that delegate for scripts to add to.
	public PowerUpDelegate OnPowerUp;
}
