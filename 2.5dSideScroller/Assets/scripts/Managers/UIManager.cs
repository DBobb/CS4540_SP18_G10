﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This manager is to behave as a Persistant Singleton. Meaning there should only be one of it's kind, and it'll be handling UI.
//A persistant singleton exists between scenes as well.

// Author: Robert Bobbitt
// Time spent: 2 hours
// User Story: 11
// Class Purpose:Changes the active health and mana slider.

public class UIManager : Singleton<UIManager> {


	// ****** Character Health Slider Variables ******
	public Slider char1HealthSlider;
	public Slider char2HealthSlider;
	public Slider char3HealthSlider;
	public Slider curCharHealthSlider;

	// ****** Character Mana Slider Variables ******
	public Slider Char1ManaSlider;
	public Slider Char2ManaSlider;
	public Slider Char3ManaSlider;
	public Slider curCharManaSlider;

    public Slider Char1ManaCoolDownSlider;
    public Slider Char2ManaCoolDownSlider;
    public Slider Char3ManaCoolDownSlider;
    public Slider Char1HealthCoolDownSlider;
    public Slider Char2HealthCoolDownSlider;
    public Slider Char3HealthCoolDownSlider;

    SliderImageTransform sit;

	GameObject sliderTrans;
	GameObject sliderTransImage;

	GameObject char1;
	GameObject char2;
	GameObject char3;
	GameObject curChar;

	 void Start () {
        //Calling original awake funcion
        //base.Awake ();
        curCharHealthSlider = char1HealthSlider;
        curCharManaSlider = Char1ManaSlider;

        char1 = CharacterManager.Instance.char1;
		char2 = CharacterManager.Instance.char2;
		char3 = CharacterManager.Instance.char3;
		curChar = CharacterManager.Instance.curChar;

		sit = UIManager.Instance.GetComponent<SliderImageTransform>();

		//st = sliderTrans.GetComponent<SliderTransform>();
		//sit = sliderImageTrans.GetComponent<SliderImageTransform>();

		HealthBarSetup();
		ManaBarSetup();
	}

	// Use this for initialization
	//void Start () {
		// Set char1's health and mana slider to the current slider

	//}

	// Update is called once per frame
	void Update () {
		 curChar = CharacterManager.Instance.curChar.gameObject;
		RegenerateMana ();
		if (CharacterManager.Instance.swap == true) {

			sit.RevertImage(curCharHealthSlider, curCharManaSlider);
			SwapManaAndHealth();

			if (CharacterManager.Instance.curChar == CharacterManager.Instance.char1.gameObject) {
				curCharHealthSlider = char1HealthSlider;
				curCharManaSlider = Char1ManaSlider;
			}
			else if (CharacterManager.Instance.curChar == CharacterManager.Instance.char2.gameObject) {
				curCharHealthSlider = char2HealthSlider;
				curCharManaSlider = Char2ManaSlider;
			}
			else if (CharacterManager.Instance.curChar == CharacterManager.Instance.char3.gameObject) {
				curCharHealthSlider = char3HealthSlider;
				curCharManaSlider = Char3ManaSlider;
			}
		}
	}



	void HealthBarSetup()
	{
		char1HealthSlider.maxValue = CharacterManager.Instance.char1.GetComponent<CharacterHealth>().startingHealth;
		char1HealthSlider.value = CharacterManager.Instance.char1.GetComponent<CharacterHealth>().startingHealth;;

		char2HealthSlider.maxValue = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth;;
		char2HealthSlider.value = CharacterManager.Instance.char2.GetComponent<CharacterHealth>().startingHealth;;

		char3HealthSlider.maxValue = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth;;
		char3HealthSlider.value = CharacterManager.Instance.char3.GetComponent<CharacterHealth>().startingHealth;;
	}

	void ManaBarSetup()
	{
		Char1ManaSlider.maxValue = CharacterManager.Instance.char1.GetComponent<CharacterMana> ().startingMana;
		Char1ManaSlider.value = CharacterManager.Instance.char1.GetComponent<CharacterMana> ().startingMana;

		Char2ManaSlider.maxValue = CharacterManager.Instance.char1.GetComponent<CharacterMana> ().startingMana;
		Char2ManaSlider.value = CharacterManager.Instance.char1.GetComponent<CharacterMana> ().startingMana;

		Char2ManaSlider.maxValue = CharacterManager.Instance.char2.GetComponent<CharacterMana> ().startingMana;
		Char2ManaSlider.value = CharacterManager.Instance.char2.GetComponent<CharacterMana> ().startingMana;

	}

	void RegenerateMana()
	{
		// If Char 1 is not active, and their mana is not full, regenerate their mana
		if (CharacterManager.Instance.char1.activeSelf == false && Char1ManaSlider.value < CharacterManager.Instance.char1.GetComponent<CharacterMana>().startingMana)
		{
			Char1ManaSlider.value += CharacterManager.Instance.char1.GetComponent<CharacterMana>().regenMana;

        }

		// If Char 2 is not active, and their mana is not full, regenerate their mana
		if (CharacterManager.Instance.char2.activeSelf == false && Char2ManaSlider.value < CharacterManager.Instance.char2.GetComponent<CharacterMana>().startingMana)
		{
			Char2ManaSlider.value += CharacterManager.Instance.char2.GetComponent<CharacterMana>().regenMana;

		}

		// If Char 3 is not active, and their mana is not full, regenerate their mana
		if (CharacterManager.Instance.char3.activeSelf == false && Char3ManaSlider.value < CharacterManager.Instance.char3.GetComponent<CharacterMana>().startingMana)
		{
			Char3ManaSlider.value += CharacterManager.Instance.char3.GetComponent<CharacterMana>().regenMana;

		}
	}

	void SwapManaAndHealth()
	{
		if (CharacterManager.Instance.curChar == CharacterManager.Instance.char1)
		{
			curCharHealthSlider = char1HealthSlider;
			curCharManaSlider = Char1ManaSlider;

			sit.UpdateImage(curCharHealthSlider, curCharManaSlider);
		}
		else if (CharacterManager.Instance.curChar == CharacterManager.Instance.char2)
		{
			curCharHealthSlider = char2HealthSlider;
			curCharManaSlider = Char2ManaSlider;

			sit.UpdateImage(curCharHealthSlider, curCharManaSlider);
		}
		else if (CharacterManager.Instance.curChar == CharacterManager.Instance.char3)
		{
			curCharHealthSlider = char3HealthSlider;
			curCharManaSlider = Char3ManaSlider;

			sit.UpdateImage(curCharHealthSlider, curCharManaSlider);
		}
	}
}
