﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewEnemyAttack : MonoBehaviour {

    public float attackDist;       //Distance to attack player
    public float chaseDist;		   //Distance to chase player
    public float chaseSpeed;	   //Speed of chase	
    public float patrolSpeed;      //Speed of patrol walk
	public float maxDist;          //The maximum distance the enemy can follow the player

    //string attackName = "Attack";
    public int damage;                // Amount of damage this attack deals to enemis
    public float startActiveTime;       // A time when the hitbox object first become active during the animation
    public float endActiveTime;         // A time when the hitbox object becomes inactive during the animation

	public float patrolRange;

    public AnimationClip attackAnimation;      // Specific attack clip pulled from PlayerAC
    public Collider attackBox;          // A collider that is attached to the character and is disabled upon spawn

    float startTime = 0f;               // Time tracker to note beginning of attack
    bool isAttacking = false;           // Trigger variable
	bool facingRight = true;			// Bool for facing
	bool retreat = false;			    // Bool for if enemy is retreating

	float startPosition;                // Stores enemy start location
	float direc = 1;					// Stores the direction of player (1 = right) (-1 = left)

    Animator animator; // EnemyAC
    Rigidbody myRB;  //EnemyRigidbody

    // Use this for initialization
    void Start () {
		startPosition = this.transform.position.x;
        startActiveTime = startActiveTime * attackAnimation.length;
        endActiveTime = endActiveTime * attackAnimation.length;
        animator = GetComponent<Animator>();
        myRB = GetComponent<Rigidbody>();
        attackBox.enabled = false;

    }
	
	// Update is called once per frame
	void FixedUpdate () {
		
		if (Mathf.Abs (this.transform.position.x - CharacterManager.Instance.curChar.transform.position.x) < attackDist) { // Char within attack range
			Attack ();
			animator.SetBool ("Chase", false);

		} else if (Mathf.Abs (this.transform.position.x - CharacterManager.Instance.curChar.transform.position.x) < chaseDist) {//&& Mathf.Abs(this.transform.position.x - startPosition) < maxDist) // Char needs to be followed
			Chase ();
			animator.SetBool ("Attack", false);

		} else if (retreat)  {
			Return ();
		}
        else // Else just return to normal walking pace
        {
            Patrol();
        }

	}

	//This function enables a box collider that damages the player given the enemy is in a certain range.
    void Attack()
    {
        // Conditions Required to begin attacking
        if (!isAttacking)
        {
            startTime = Time.time; // Get start time of when attack began
            animator.SetBool("Attack", true);
            animator.SetBool("Chase", false);
            isAttacking = true;
        }
        // Code to perform if character is attacking
        else if (isAttacking == true)
        {
            // Disable certain scripts
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            
            // Execute this while the attack animation lasts
            if (Time.time - startTime < attackAnimation.length)
            {
                

                // if we are at the active attacking times
                if (Time.time - startTime >= startActiveTime && Time.time - startTime < endActiveTime)
                {

                    attackBox.enabled = false;
                }
                // if we have exceeded the attacking times
                else if (Time.time - startTime > endActiveTime)
                {
                    attackBox.enabled = true;
                }
            }
            // Execute this after attack animation finishes
            else if (Time.time - startTime >= attackAnimation.length)
            {
                isAttacking = false;

                animator.SetBool("Attack", false);
                attackBox.enabled = false;
            }
        }
    }

	//This function makes the enemy chase the player given the player enters a certain range.
    void Chase()
    {
		attackBox.enabled = false;
		float dist = Mathf.Abs (this.transform.position.x - startPosition); 
        float direc = Mathf.Sign(CharacterManager.Instance.curChar.transform.position.x - this.transform.position.x);
        this.myRB.velocity = new Vector3(direc * chaseSpeed, 0, 0);
        animator.SetBool("Chase", true);
		// Using Inputs and Hspeed to Filp model in animator
		if (direc > 0 && !facingRight)
			FlipModel();
		else if (direc < 0 && facingRight)
			FlipModel();

		if (dist > maxDist) {
			retreat = true;
		}
    }

	// This function makes the enemy walk back and forth from his start location simulating a patrolling guard.
    void Patrol()
    {
		attackBox.enabled = false;
		float dist = Mathf.Abs (this.transform.position.x - startPosition); 

		if (dist > patrolRange)
			direc = -direc;

		this.myRB.velocity = new Vector3 (direc * patrolSpeed, 0, 0);
		animator.SetBool ("Patrol", true);

		if (direc > 0 && !facingRight)
			FlipModel();
		else if (direc < 0 && facingRight)
			FlipModel();
    }

	//This function flips the model of it's object.  It's used to make sure the animations look right mostly.
	void FlipModel()
	{
		facingRight = !facingRight; //change our facing boolean to reflect current direction we're facing
		Vector3 modelScale = transform.localScale;
		modelScale.z *= -1;
		transform.localScale = modelScale;
	}

	// This function tells the enemy to return to their start position.
	void Return()
	{
		float direc = Mathf.Sign(startPosition - this.transform.position.x);
		float dist = Mathf.Abs (this.transform.position.x - startPosition);

		if (dist < 1 && dist > 0) {
			retreat = false;
			animator.SetBool ("Chase", false);
		}
		else  
			this.myRB.velocity = new Vector3 (direc * chaseSpeed, 0, 0);
 			
	
		if (direc > 0 && !facingRight)
			FlipModel();
		else if (direc < 0 && facingRight)
			FlipModel();
		
	}
}
