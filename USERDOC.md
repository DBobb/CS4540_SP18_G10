## CORE MECHANICS

- Welcome to Adventures of the Crystal Campaign (AotCC)!
- AotCC is about 3 main characters that you can play as, all of which have different capabilities and weakness that you will need to take advantage of.
- This game is a 2.5D side-scrolling metroidvania game. 

## BASIC CONTROLS

- Using a controller: 

	Left Bumper - Switch up a character

	Right Bumper - Switch down a character

	Start Button - Pause button

	(Y) Button - Character's heavy attack

	(X) Button - Character's light attack

	(B) Button - Roll/ Back button

	(A) Button - Jump/ Interact button

	D-pad Buttons - Use items in temporary inventory or move them to your main inventory while at a lodestone

	Thumbstick - Move the current character

- Using a keyboard (NOT RECOMMENDED):

	Moving: Use the arrow keys to move left or right, or the 'a' or 'd' key. 

	Jumping: Use the space bar to jump.

	Switch character: Use '1' to switch to character below them (If currently character 2, use '1' to switch to character 1), 
		  	  use '2' to switch to character above them. 

	Light attack: Use 'o' to perform a light attack when near an enemy to damage them.

	Heavy attack: Use 'p' to perform a heavy attack when near an enemy to damage them.

	Light air attack: Use 'o' while in the air to perform a light air attack when near an enemy to damage them.

	Heavy air attack: Use 'p' while in the air to perform a heavy air attack when near an enemy to damage them.

- Temporary Inventory Controls: 

	Collecting the inventory items: As the character collides with a collectable item, it will be added to the inventory if there is space.

	Using the inventory items: Use the arrow keys on the num pad (make sure num lock is enabled), press 5 for slot 0, 4 for slot 1, 8 for slot 2 and 6 for slot 3.

- LodeStone Controls: 
	
	Lode Stones: Interact with a lode stone by pressing 'i' near one. This will trigger the lode stone menu. To exit this menu, press the 'esc' key.

	Lode Stone Menu: While on the lode stone menu, use the arrow keys to navigate, use 'm' to press buttons and the normal inventory controls to remove things from the inventory.

	Level Up Box: While on the level up box, use the arrow keys to navigate the menu, use 'm' to confirm an upgrade. 

	Item Box: While on the item box, use the arrow keys on the num pad to transfer items from your temporary inventory to the main inventory.
	  Use the arrow keys (not on the num pad) to navigate the main inventory list. Press 'm' on a filled slot to move it back to the temporary inventory.

	Warp Box: While on the warp box, use the arrow keys to navigate the previously visited lodestone list. Press 'm' on a filled slot to warp to that lodestone.

	Save Box: While on the save box, press 'm' on the save slot. If the save was successful, a pop up saying 'Save Successful' will display.

	Convert Box: While on the convert box, press 'm' on a nonempty slot to convert that item to soft bismuth. 

	To Exit: Iteract with a lode stone and naviage to the exit button, press 'm', (make sure to save first!).


## GAMEPLAY BASICS

- Switching Characters

	* Switching out a character will regenerate their mana

	* There is a cooldown when a character is switched out, therefore you have to wait a few seconds to switch back to them (this is indicated on the health/mana UI)

- Attacking

	* A heavy attack will cause more damage to an enemy than a light attack

	* Using a heavy attack will use the current character's mana

	* Both attacks can be used in the air as well to perform a different action

- Using Your Items

	* Throughout the world there will be items to be found on the ground, they will be white and glowing

	* Pick them up by walking right over them

	* Calcite can be used to restore your current character's health (the icon is a health vile)

	* You can move your temporary inventory items into your main inventory by using the lode stone menu

- Soft Bismuth

	* Soft bismuth is a type of currency to level up your characters' stats

	* It cannot be physically collected, but you do gain soft bismuth when an enemy dies

	* You can also convert your calcite to softbismuth in the lodestone menu

- Using the Lodestone

	* Throuhout the game, there are large crystals called lodestones

	* All your characters will be healed everytime you interact with the lodestone, and their mana will be filled

	* From here you can do many different actions such as convert to soft bismuth, level up characters, save, exit, warp and move inventory items around

	* You will lose all soft bismuth if Amelia is killed

	* You will spawn on the last lode stone you visited if you die or press continue game from the main menu

- Death

	* Each character will have a set amount of health, however if Amelia (character 1) dies, you lose the game and all the progress made after your last save

- Saving 

	* You can save at any lodestone in the game

	* You will spawn at the lodestone furthest into the game that you saved at (ex. if you save in level 2, then warp back to level 1 and save again, you will still spawn in level 2)

- Level Design

	* Some levels require certain characters in order to proceed (ex. Itzuli may be needed to get to a certain platform, as he jumps the highest)

	* There are purple firey gates that will move you to the next level, or back to a previous level
	