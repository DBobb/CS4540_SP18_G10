## Authors: 

Keaton Smith, Drew Bobbitt and ALivia Johnson

    
## Version: 

0.4

    
## Compatibility: 

PC with gaming controller/ keyboard

RECOMMENDED TO USE WITH CONTROLLER FOR FULL FUNCTIONALITY


## To Run:

Run the CrystalCampaignBuild.exe, located in the main directory, CS4540_SP18_G10.

Recommended screen size is 1920 X 1080. 

      
## Project Summary: 

This project is an exploration based 2.5D platformer developed in Unity. 


## Requirements:

Unity Version 2017.3.0f3 or higher.
    

## Code Style:

We follow Unity Conventions - https://unity3d.com/learn/tutorials/topics/scripting/conventions-and-syntax.

    
## Game Objects:

Prefabs are found in Assets/Prefabs folder.   

Characters Folder - Contains prefabs of the three characters. 

EnemyPrefabs Folder - Contains prefabs of the enemies. 

Health&ManaBars Folder - Contains all UI elements and prefabs used to display the health and mana bars.

Inventory Folder - Contains all UI elements and prefabs used to display the inventory. 

LevelPlatforms Folder - Contains one and two way platform prefabs. 

LoadStoneMenuUI Folder - Contains all UI elements and prefabs used in the lodestone. 

			 ConvertBox Folder - Contains UI elements for the convert box.

			 ExitBox Folder - Contains UI elements for the exit box. 

			 ItemBox Folder - Contains UI elements for the item box. 

			 LevelUpBox Folder - Contains UI elements for the level up box.

			 MainInventory Folder - Contains UI elements for the main inventory.

			 SaveMenuBox Folder - Contains UI elements for the save box. 

			 WarpBox Folder - Contains UI elements for the warp box. 

MainMenu Folder - Contains all UI elements and prefabs used on the main menu. 
		  Options Folder - Contains all UI elements used on the options menu. 

Managers Folder - Contains the prefabs for all the managers used in the game. 

UIElements Folder - Contains the random UI elements that did not fall into an above category.


## Class Names:

Capitalized first letter + camelCase. (EX: GameObject).
        
    
## Scripts: 

First part is object to be attached to, second is functionality it provides. (EX: A script for character jumping functionality is CharacterJump).

Visual Studios and MonoDeveloper were used to write and test these scripts.

Scripts are all located in Assets/Scripts folder.

AI Folder - Contains all enemy AI scripts. 
	    States Folder - Contains enemy attack states.

Characters Folder - Contains all character scripts

	   	    AmeliaScripts Folder - Contains all scripts unique to Amelia.

		    ItzuliScripts Folder - Contains all scripts unique to Itzuli.

Enemy Folder - Contains all enemy attack scripts. 

Inventory Folder - Contains all scripts that allow the inventory to work. 

LoadStoneMenuUI Folder - Contains all scripts that allow the lodestone menu to work.

			 ConvertBox Folder - Contains all scripts that allow converting.

			 ItemBox Folder - Contains all scripts that allow the main inventory to work.

			 LevelUpBox Folder - Contains all scripts for the characters' leveling up system. 

			 SaveMenuBox Folder - Contains all scripts that allow saving in game.

			 WarpBox Folder - Contains all scripts that allow the player to warp.

MainMenu Folder - Contains all scripts that allow the menu to be interactable. 

		  Options Folder - Contains the scripts for the options menu. 

Manager Folder - Contains all scripts that are managers in the game. 

OnScreenUI Folder - Contains all scripts that deal with UI items on screen (except for the inventory). 

PickUpItems Folder - Script that spawns pickup items (calcite, cuprite).

SliderBarUI Folder - Script that transforms the UI Sliders when health/ mana is lost or gained.

Spawners Folder - Contains scripts for spawning enemies. 
