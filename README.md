## Game Title:

Adventures of the Crystal Campaign


## Developers:

Alivia Johnson, Robert Bobbitt and Roy Keaton Smith


## Artists:

Monica Fannin and Samuel Molloy


## Game Overview:

This project consists of creating a 2.5d side scroller game. The player is able to swap between 3 playable characters that have separate health and mana bars. Each character will have a different set of skills, such as speed, jump height, and attack type. The player will encounter several enemies throughout the level. The player can use the different characters to defeat these enemies, as one character's attack type might be more effective against a certain enemy type. The player can also explore levels and find collectables, which will be put into the player's inventory. The player can save at loadstones that are placed throughout the level. Finally, the player will be challenged with a final level boss, which they can use all three characters to defeat.


## Player Controls (Keyboard): 

Main Controls: 
Moving: Use the arrow keys to move left or right, or the 'a' or 'd' key. 

Jumping: Use the space bar to jump.

Switch character: Use '1' to switch to character below them (If currently character 2, use '1' to switch to character 1), 
		  use '2' to switch to character above them. 

Light attack: Use 'o' to perform a light attack when near an enemy to damage them.

Heavy attack: Use 'p' to perform a heavy attack when near an enemy to damage them.

Light air attack: Use 'o' while in the air to perform a light air attack when near an enemy to damage them.

Heavy air attack: Use 'p' while in the air to perform a heavy air attack when near an enemy to damage them.

Temporary Inventory Controls: 
Collecting the inventory items: As the character collides with a collectable item, it will be added to the inventory if there is space.

Using the inventory items: Use the arrow keys on the num pad (make sure num lock is enabled), press 5 for slot 0, 4 for slot 1, 8 for slot 2 and 6 for slot 3.

LodeStone Controls: 
Lode Stones: Interact with a lode stone by pressing 'i' near one. This will trigger the lode stone menu. To exit this menu, press the 'esc' key.

Lode Stone Menu: While on the lode stone menu, use the arrow keys to navigate, use 'm' to press buttons and the normal inventory controls to remove things from the inventory.

Level Up Box: While on the level up box, use the arrow keys to navigate the menu, use 'm' to confirm an upgrade. 

Item Box: While on the item box, use the arrow keys on the num pad to transfer items from your temporary inventory to the main inventory.
	  Use the arrow keys (not on the num pad) to navigate the main inventory list. Press 'm' on a filled slot to move it back to the temporary inventory.

Warp Box: While on the warp box, use the arrow keys to navigate the previously visited lodestone list. Press 'm' on a filled slot to warp to that lodestone.

Save Box: While on the save box, press 'm' on the save slot. If the save was successful, a pop up saying 'Save Successful' will display.

Convert Box: While on the convert box, press 'm' on a nonempty slot to convert that item to soft bismuth. 

To Exit: Iteract with a lode stone and naviage to the exit button, press 'm', (make sure to save first!).

Main menu: To continue a previously saved game, press the continue button, to start a new game, press new game.


## Player Controls (Controller)

Left Bumper - Switch up a character

Right Bumper - Switch down a character

Start Button - Pause button

(Y) Button - Character's heavy attack

(X) Button - Character's light attack

(B) Button - Roll/ Back button

(A) Button - Jump/ Interact button

D-pad Buttons - Use items in temporary inventory or move them to your main inventory while at a lodestone

Thumbstick - Move the current character


## Items

Calcite: The current character can restore their health by using a calcite item located in the inventory. Can be converted to soft bismuth in the lode stone menu.

Cuprite: Can be converted to soft bismuth in the lode stone menu.

Soft Bismuth: Dropped when enemies are defeated, counter is on the top right corner of the screen. Can be used to level up characters in teh lode stone menu.


## Motivation:

This project was inspired by artists Monica and Samuel, who wanted to make a game which focused on the separation of social and economical classes. The developers were inspired to create this game because it follows a desirable game type: 2.5 side scroller, which is similar in style as many other games they played before.
## Installation:

This project will be played by running the exectutable when the game is completed, or even available on a software distribution platform. This project is available on this git repository, and it can be modified using Unity 2017.
